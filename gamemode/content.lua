local DefaultWorkshopContent = {
    "2477378914", -- Space Combat 2
    "160250458", -- Wiremod
}

local DefaultContentPaths = {
    SC.DataFolder.."/clientcontent"
}

local UsersWaitingForCotentList = {}
local WorkshopContentToLoad = {}
local FilesToSend = {}
local ShouldUsersDownloadContent = true
local ContentSettingsLoaded = false

local function AddFilesRecursive(Path)
    print("i find things", Path)
    local Files, Directories = file.Find(Format("%s/*", Path), "DATA")
    for _, File in pairs(Files) do
        local FileInfo = {}
        FileInfo.Path = Format("%s/%s", Path, File)
        print(FileInfo.Path)

        local FileData = util.Compress(file.Read(FileInfo.Path, "DATA"))
        if FileData:len() < 54000 then
            FileInfo.Data = FileData
        else
            FileInfo.NumParts = math.ceil(FileData:len() / 54000)
            FileInfo.FileParts = {}
            for I = 0, (FileInfo.NumParts - 1) do
                local NextData = FileData:sub(I * 54000, ((I + 1) * 54000) - 1)
                table.insert(FileInfo.FileParts, NextData)
            end
        end

        table.insert(FilesToSend, FileInfo)
    end

    for _, Dir in pairs(Directories) do
        AddFilesRecursive(Format("%s/%s", Path, Dir))
    end
end

local function SendContentList(ply)
    if ContentSettingsLoaded then
        net.Start("SC.GetContentList")
        net.WriteBool(ShouldUsersDownloadContent)
        if ShouldUsersDownloadContent then
            net.WriteUInt(table.Count(FilesToSend), 16)
            net.WriteTable(WorkshopContentToLoad)
        end
        net.Send(ply)

        if ShouldUsersDownloadContent then
            local SendDelay = 0.05
            local SendIndex = 1
            for _, FileInfo in ipairs(FilesToSend) do
                if FileInfo.NumParts and FileInfo.NumParts > 1 then
                    for Index, FilePart in ipairs(FileInfo.FileParts) do
                        timer.Simple(SendIndex * SendDelay, function()
                            net.Start("SC.SendContentFilePart")
                            net.WriteString(FileInfo.Path)
                            net.WriteUInt(FileInfo.NumParts, 8)
                            net.WriteUInt(Index, 8)
                            local Size = FilePart:len()
                            net.WriteUInt(Size, 16)
                            net.WriteData(FilePart, Size)
                            net.Send(ply)
                        end)

                        SendIndex = SendIndex + 1
                    end
                else
                    timer.Simple(SendIndex * SendDelay, function()
                        net.Start("SC.SendContentFile")
                        net.WriteString(FileInfo.Path)
                        local Size = FileInfo.Data:len()
                        net.WriteUInt(Size, 16)
                        net.WriteData(FileInfo.Data, Size)
                        net.Send(ply)
                    end)

                    SendIndex = SendIndex + 1
                end
            end
        end
    else
        table.insert(UsersWaitingForCotentList, ply)
    end
end

net.Receive("SC.GetContentList", function(len, ply)
    SendContentList(ply)
end)

local function LoadContentConfig()
    local LoadDefaultData = false
    if file.Exists(SC.DataFolder.."/config/content.txt", "DATA") then
        local SaveData = lip.load(SC.DataFolder.."/config/content.txt")

        if SaveData["Config"] then
            ShouldUsersDownloadContent = SaveData["Config"]["ShouldUsersDownloadContent"] or false
        else
            LoadDefaultData = true
        end

        if ShouldUsersDownloadContent then
            if SaveData["WorkshopContentList"] then
                WorkshopContentToLoad = SaveData["WorkshopContentList"]
            end

            if SaveData["DataContentPaths"] then
                local SearchPaths = SaveData["DataContentPaths"]
                for _, Path in pairs(SearchPaths) do
                    AddFilesRecursive(Path)
                end
            end
        end
    else
        LoadDefaultData = true
    end

    if LoadDefaultData then
        local SaveData = {}

        SaveData["Config"] = {
            ShouldUsersDownloadContent = ShouldUsersDownloadContent
        }

        SaveData["WorkshopContentList"] = DefaultWorkshopContent
        SaveData["DataContentPaths"] = DefaultContentPaths

        WorkshopContentToLoad = WorkshopContentToLoad

        local SearchPaths = SaveData["DataContentPaths"]
        for _, Path in pairs(SearchPaths) do
            AddFilesRecursive(Path)
        end

        file.CreateDir(SC.DataFolder.."/config/")
        lip.save(SC.DataFolder.."/config/content.txt", SaveData)
    end

    ContentSettingsLoaded = true

    hook.Run("SC.Content.PostConfig", ShouldUsersDownloadContent, WorkshopContentToLoad)

    for _, Ply in ipairs(UsersWaitingForCotentList) do
        SendContentList(Ply)
    end
end
hook.Remove("SC.Config.PostLoad", "SC.Content.Setup")
hook.Add("SC.Config.PostLoad", "SC.Content.Setup", LoadContentConfig)

util.AddNetworkString("SC.GetContentList")
util.AddNetworkString("SC.SendContentFile")
util.AddNetworkString("SC.SendContentFilePart")