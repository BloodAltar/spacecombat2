--[[

SUI Scoreboard v2.6 by .Z. Nexus is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
----------------------------------------------------------------------------------------------------------------------------
Copyright (c) 2014 .Z. Nexus <http://www.nexusbr.net> <http://steamcommunity.com/profiles/76561197983103320>

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en_US.
----------------------------------------------------------------------------------------------------------------------------
This Addon is based on the original SUI Scoreboard v2 developed by suicidal.banana.
Copyright only on the code that I wrote, my implementation and fixes and etc, The Initial version (v2) code still is from suicidal.banana.
----------------------------------------------------------------------------------------------------------------------------

$Id$
Version 2.6.2 - 12-06-2014 05:33 PM(UTC -03:00)

]]--

--[[==========================
	DIASPORA SCOREBOARD
	EDIT BY STEEVEEO
===========================--]]

AddCSLuaFile()

-- Setup Class
Scoreboard = {}

if SERVER then
	-- For Players to Download this addon from Workshop.
	--resource.AddWorkshop("160121673")

	AddCSLuaFile()
	hook.Add("PlayerInitialSpawn", "SUISCOREBOARD-Spawn", Scoreboard.PlayerSpawn)
	
	-- Add to the pool
	util.AddNetworkString("SUIScoreboardPlayerColor")

	-- Send required files to client
	AddCSLuaFile()
	AddCSLuaFile("scoreboard/client/scoreboard.lua")
	AddCSLuaFile("scoreboard/client/admin_buttons.lua")
	AddCSLuaFile("scoreboard/client/tooltips.lua")
	AddCSLuaFile("scoreboard/client/player_frame.lua")
	AddCSLuaFile("scoreboard/client/player_infocard.lua")
	AddCSLuaFile("scoreboard/client/player_row.lua")
	AddCSLuaFile("scoreboard/client/scoreboard.lua")
	AddCSLuaFile("scoreboard/client/vote_button.lua")
	AddCSLuaFile("scoreboard/client/library.lua")
	AddCSLuaFile("scoreboard/client/netClient.lua")
	include( "scoreboard/server/rating.lua" )
	include( "scoreboard/server/library.lua" )
	
	timer.Create("Scoreboard_UpdatePlayerTime", 1, 0, function()
		for k,v in pairs(player.GetAll()) do
			if IsValid(v) then
				v:SetNWInt("TimeConnected", v:TimeConnected())
			end
		end
	end)
else
	Scoreboard.vgui = nil
	Scoreboard.playerColor = Color(255, 155, 0, 255)
	include( "scoreboard/client/library.lua" )
	include( "scoreboard/client/scoreboard.lua" )
	include( "scoreboard/client/netClient.lua" )

	hook.Add("ScoreboardShow","SUISCOREBOARD-Show", Scoreboard.Show)
	hook.Add("ScoreboardHide", "SUISCOREBOARD-Hide", Scoreboard.Hide)
end