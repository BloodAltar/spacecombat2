local GM = GM
local CreateConVar = CreateConVar
local GetConVar = GetConVar
local game = game

CreateConVar("SB_NoClip", "1")
CreateConVar("SB_AdminSpaceNoclip", "1") -- Makes it so admins can no clip in space, defaults to yes
CreateConVar("SB_PlanetNoClipOnly", "1") -- Make it so admins can let players no clip in space.
CreateConVar("SB_EnableDrag", "1") -- Make it drag also gets affected, on by default.
CreateConVar("SB_InfiniteResources", "0") -- Makes it so that a planet can't run out of resources, off by default.

-- Physics convars
local MaxVelocity = CreateConVar("phys_maxvelocity", "10000", bit.bor(FCVAR_NOTIFY, FCVAR_ARCHIVE, FCVAR_GAMEDLL))
local MaxAngularVelocity = CreateConVar("phys_maxangularvelocity", "1800", bit.bor(FCVAR_NOTIFY, FCVAR_ARCHIVE, FCVAR_GAMEDLL))
local GravityX = CreateConVar("phys_gravity_x", "0", bit.bor(FCVAR_NOTIFY, FCVAR_GAMEDLL))
local GravityY = CreateConVar("phys_gravity_y", "0", bit.bor(FCVAR_NOTIFY, FCVAR_GAMEDLL))
local GravityZ = CreateConVar("phys_gravity_z", "-600", bit.bor(FCVAR_NOTIFY, FCVAR_GAMEDLL))
local CollisionsPerObjectPerTimestep = CreateConVar("phys_collisions_object_timestep", "10", bit.bor(FCVAR_NOTIFY, FCVAR_GAMEDLL))
local CollisionsPerTimestep = CreateConVar("phys_collisions_timestep", "150", bit.bor(FCVAR_NOTIFY, FCVAR_GAMEDLL))
local MinFrictionMass = CreateConVar("phys_minfrictionmass", "10", bit.bor(FCVAR_NOTIFY, FCVAR_GAMEDLL))
local MaxFrictionMass = CreateConVar("phys_maxfrictionmass", "2500", bit.bor(FCVAR_NOTIFY, FCVAR_GAMEDLL))
local AirDensity = CreateConVar("phys_airdensity", "2", bit.bor(FCVAR_NOTIFY, FCVAR_GAMEDLL))

local function UpdatePerformanceSettings()
    local settings = physenv.GetPerformanceSettings()

    settings.MaxVelocity = MaxVelocity:GetFloat()
    settings.MaxAngularVelocity = MaxAngularVelocity:GetFloat()
    settings.MinFrictionMass = MinFrictionMass:GetFloat()
    settings.MaxFrictionMass = MaxFrictionMass:GetFloat()
    settings.MaxCollisionsPerObjectPerTimestep = CollisionsPerObjectPerTimestep:GetInt()
    settings.MaxCollisionChecksPerTimestep = CollisionsPerTimestep:GetInt()

    physenv.SetPerformanceSettings(settings)
end

local function UpdateGravity()
    physenv.SetGravity(Vector(GravityX:GetFloat(), GravityY:GetFloat(), GravityZ:GetFloat()))
end

local function UpdateAirDensity()
    physenv.SetAirDensity(AirDensity:GetFloat())
end

local function InitPostEntity()
    UpdatePerformanceSettings()
    UpdateGravity()
    UpdateAirDensity()
end
hook.Add("InitPostEntity", "PhysicsSettingsStartup", InitPostEntity)

-- watch for changes on these convars
cvars.AddChangeCallback("phys_maxvelocity", UpdatePerformanceSettings)
cvars.AddChangeCallback("phys_maxangularvelocity", UpdatePerformanceSettings)
cvars.AddChangeCallback("phys_collisions_object_timestep", UpdatePerformanceSettings)
cvars.AddChangeCallback("phys_collisions_timestep", UpdatePerformanceSettings)
cvars.AddChangeCallback("phys_minfrictionmass", UpdatePerformanceSettings)
cvars.AddChangeCallback("phys_maxfrictionmass", UpdatePerformanceSettings)
cvars.AddChangeCallback("phys_gravity_x", UpdateGravity)
cvars.AddChangeCallback("phys_gravity_y", UpdateGravity)
cvars.AddChangeCallback("phys_gravity_z", UpdateGravity)
cvars.AddChangeCallback("phys_airdensity", UpdateAirDensity)