AddCSLuaFile()

local Admin = SC.Administration

hook.Add("RegisterSC2Commands", "RegisterProtectionCommands", function()
    -- Register any permissions we need for these commands
    Admin.RegisterPermission("ForcePVP", "Allows a user to forcefully change someone's PVP status.")

    -- Register commands
    Admin.RegisterCommand("PVP", "Change your PVP status.",
        -- Permissions
        {

        },

        -- Arguments
        {
            {
                Name = "Enabled",
                Type = "bool"
            },
        },

        -- Callback
        function(Executor, Arguments)
            local Enabled = Arguments.Enabled

            if IsValid(Executor) then
                Executor.SC_PVPMODE = Enabled

                if Enabled then
                    Executor:ChatPrint("PVP is now enabled!", "Notices")
                else
                    Executor:ChatPrint("PVP is now disabled!", "Notices")
                end
            end
        end)

    Admin.RegisterCommand("ForcePVP", "Change your PVP status.",
        -- Permissions
        {
            "ForcePVP"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            },
            {
                Name = "Enabled",
                Type = "bool"
            },
        },

        -- Callback
        function(Executor, Arguments)
            local Target = Arguments.Target
            local Enabled = Arguments.Enabled

            if IsValid(Target) then
                Target.SC_PVPMODE = Enabled

                if Enabled then
                    Target:ChatPrint("PVP is now enabled!", "Notices")
                    Executor:ChatPrint("PVP is now enabled!", "Notices")
                else
                    Target:ChatPrint("PVP is now disabled!", "Notices")
                    Executor:ChatPrint("PVP is now disabled!", "Notices")
                end
            end
        end)
end)

if SERVER then
    hook.Add("SC.ShouldApplyDamage", "SC.PVPMode.ShouldApplyDamage", function(self, Damage, Attacker, Inflictor, HitData)
        if not IsValid(Attacker) then
            return
        end

        if not Attacker:IsPlayer() then
            return
        end

        if Attacker == self then
            return
        end

        if self:IsPlayer() then
            return self.SC_PVPMODE
        end

        local ToCheck = self
        if self:IsProtected() then
            ToCheck = self:GetProtector()
        end

        local Space = GAMEMODE:GetSpace()
        if ToCheck.GetEnvironment and ToCheck:GetEnvironment() == Space then
            if not Space:GetHabitable() then
                return
            end
        end

        local Owner
        if CPPI and ToCheck.CPPIGetOwner then
			Owner = ToCheck:CPPIGetOwner()
		end

		if not IsValid(Owner) then
			--this is the system that garrysmod uses by default, so let's just dig into it a little
			if ToCheck.GetPlayer then Owner = ToCheck:GetPlayer() end
			if not IsValid(Owner) then Owner = ToCheck.Owner end --McBuild ownership
			if not IsValid(Owner) then Owner = ToCheck.SPL end --SBEP's ownership
			if not IsValid(Owner) and ToCheck.GetOwner then Owner = ToCheck:GetOwner() end --Sometimes
            if not IsValid(Owner) then return end
		end

        if Attacker == Owner then
            return
        end

        return Owner.SC_PVPMODE
    end)

    hook.Add("PlayerInitialSpawn", "SC.PVPMode.PlayerInitialSpawn", function(Ply)
        Ply.SC_PVPMODE = false
    end)
end