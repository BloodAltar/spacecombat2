AddCSLuaFile()

local Admin = SC.Administration

if SERVER then
	local afkkicktime = 1200 -- THIS IS THE AFK TIME (sec)
	util.AddNetworkString("SC.Admin.WAKEUP")

	local function ResetAFKTimer( ply )
		if not IsValid(ply) then return end

		if timer.Exists("AFK_"..ply:SteamID()) then
            timer.Remove("AFK_"..ply:SteamID())
            timer.Remove("AFK_WAKEUP_"..ply:SteamID())
        end

		local function KickMe( ply )
			if IsValid(ply) then
				RunConsoleCommand( "kickid", ply:UserID(), "Idle for "..afkkicktime.." seconds." )
			end
		end

        timer.Create("AFK_WAKEUP_"..ply:SteamID(), afkkicktime - 60, 1, function() net.Start("SC.Admin.WAKEUP") net.Send(ply) end)
		timer.Create("AFK_"..ply:SteamID(), afkkicktime, 1, function() KickMe(ply) end)
	end

    hook.Add("PlayerButtonDown", "PlayerButtonDownASS", ResetAFKTimer)
    hook.Add("VGUIMousePressed", "VGUIMousePressedASS", ResetAFKTimer)
	hook.Add("KeyPress", "KeyPressedASS", ResetAFKTimer)
	hook.Add("PlayerInitialSpawn", "PlayerInitialSpawnASS", ResetAFKTimer)
else
    local function WakeUp()
        if IsValid(LocalPlayer()) then LocalPlayer():ChatPrint("You should stop being AFK! You'll be kicked in one minute.") end
        sound.PlayURL("http://dl.diaspora-community.com/wakeup.mp3", "noplay", function(sound)
	        if IsValid(sound) then
		        sound:SetPos(LocalPlayer():GetPos())
		        sound:Play()
	        end
        end)
    end

    net.Receive("SC.Admin.WAKEUP", WakeUp)
end
