local ContentToLoad = {}
local LoadedContent = {}
local IncomingFileChunks = {}
local TotalContentFiles = 0
local ReceivedContentFiles = 0

local function LoadContentConfig()
    print("requesting content list")
    net.Start("SC.GetContentList")
    net.SendToServer()
end
hook.Remove("SC.Config.PostLoad", "SC.Content.Setup")
hook.Add("SC.Config.PostLoad", "SC.Content.Setup", LoadContentConfig)

local function CheckDownloadFinished()
    local WorkshopDownloadFinished = (table.Count(LoadedContent) == table.Count(ContentToLoad))
    local DataDownloadFinished = (ReceivedContentFiles == TotalContentFiles)
    if WorkshopDownloadFinished and DataDownloadFinished then
        hook.Run("SC.Content.PostContentMounted", true, LoadedContent)
    end
end

net.Receive("SC.SendContentFile", function(len, ply)
    local Path = net.ReadString()
    local DataSize = net.ReadUInt(16)
    local Data = net.ReadData(DataSize)

    file.CreateDir(string.GetPathFromFilename(Path))
    file.Write(Path, util.Decompress(Data))
    ReceivedContentFiles = ReceivedContentFiles + 1

    CheckDownloadFinished()
end)

net.Receive("SC.SendContentFilePart", function(len, ply)
    local Path = net.ReadString()
    local TotalParts = net.ReadUInt(8)
    local PartIndex = net.ReadUInt(8)
    local PartSize = net.ReadUInt(16)
    local PartData = net.ReadData(PartSize)

    IncomingFileChunks[Path] = IncomingFileChunks[Path] or {}
    local PartList = IncomingFileChunks[Path]
    PartList[PartIndex] = PartData

    if table.Count(PartList) == TotalParts then
        local FileData = ""
        for _, Data in ipairs(PartList) do
            FileData = Format("%s%s", FileData, Data)
        end

        file.CreateDir(string.GetPathFromFilename(Path))
        file.Write(Path, util.Decompress(FileData))
        IncomingFileChunks[Path] = nil
        ReceivedContentFiles = ReceivedContentFiles + 1

        CheckDownloadFinished()
    end
end)

net.Receive("SC.GetContentList", function(len, ply)
    local ShouldMountContent = net.ReadBool()
    if ShouldMountContent then
        TotalContentFiles = net.ReadUInt(16)
        ContentToLoad = net.ReadTable()

        vgui.Create("SC.ContentProgress")

        hook.Run("SC.Content.StartedMountingContent", ContentToLoad)

        for _, Content in pairs(ContentToLoad) do
            steamworks.DownloadUGC(Content, function(Path, FileHandle)
                local Success = game.MountGMA(Path)
                if not Success then
                    SC.Error(Format("Failed to load content %s", Content), 5)
                    hook.Run("SC.Content.FailedToMount", Content)
                else
                    hook.Run("SC.Content.Mounted", Content)
                end

                table.insert(LoadedContent, Content)

                CheckDownloadFinished()
            end)
        end
    else
        hook.Run("SC.Content.PostContentMounted", false, {})
    end
end)