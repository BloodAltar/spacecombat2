local scale = 1
local visible = false
local HelpMenuFrameParent, InfoFrame, RulesFrame, ChangeFrame, TutorialFrame, WikiFrame, BugFrame, ComFrame, SteamFrame

function EaseUpFrameRate()
	if InfoFrame then InfoFrame:SetVisible(false) end
	if RulesFrame then RulesFrame:SetVisible(false) end
	if ChangeFrame then ChangeFrame:SetVisible(false) end
	if TutorialFrame then TutorialFrame:SetVisible(false) end
	if WikiFrame then WikiFrame:SetVisible(false) end
	if BugFrame then BugFrame:SetVisible(false) end
	if ComFrame then ComFrame:SetVisible(false) end
	if SteamFrame then SteamFrame:SetVisible(false) end
end

function InfoMenu()
	EaseUpFrameRate()

	if not IsValid(InfoFrame) then
		InfoFrame = vgui.Create("TacoPanel", HelpMenuFrameParent)
		InfoLabel = vgui.Create("DLabel", InfoFrame)

		local w,h = ScrW() - 210, ScrH() - 50

		InfoFrame:SetPos(105, 25)
		InfoFrame:SetSize(w, h)
		--InfoFrame:EnableTitle(false)
		InfoFrame:SetTitle("Welcome to the Diaspora Spacebuild 3 server!")
		InfoFrame:EnableDragging(false)
		InfoFrame:EnableCloseButton(false)
		InfoFrame:SetColor(Color(30, 30, 30, 255))
		InfoFrame:SetRoundness(4)

		InfoLabel:SetFont("Default")
		InfoLabel:SetPos(7, 10)
		InfoLabel:SetSize(w - 14, 100)

		InfoLabel:SetText("Diaspora is a gaming community focused mainly on Garry's Mod Spacebuild 3.\n"..
		"We have a wide range of players from around the world, lots of content, and are constantly improving!\n"..
		"\n\nThis gamemode is continuously updated by Diaspora's development team.\nOur community forums are hosted at diaspora-community.com, register today!")
	else
		InfoFrame:SetVisible(true)
	end
end

function RulesMenu()
	EaseUpFrameRate()

	if not IsValid(RulesFrame) then
		local w,h = ScrW() - 210, ScrH() - 50

		RulesFrame = vgui.Create("TacoPanel", HelpMenuFrameParent)
		RulesLabel = vgui.Create("DHTML", RulesFrame)

		RulesFrame:SetPos(105, 25)
		RulesFrame:SetSize(w, h)
		RulesFrame:EnableTitle(false)
		RulesFrame:EnableDragging(false)
		RulesFrame:EnableCloseButton(false)
		RulesFrame:SetColor(Color(30, 30, 30, 255))
		RulesFrame:SetRoundness(4)

		RulesLabel:SetPos(7, 5)
		RulesLabel:SetSize(w - 14, h - 10)

		RulesLabel:OpenURL("http://diaspora-community.com/viewtopic.php?f=8&t=8")
	else
		RulesFrame:SetVisible(true)
	end
end

function ChangeMenu()
	EaseUpFrameRate()

	if not IsValid(ChangeFrame) then
		local w,h = ScrW() - 210, ScrH() - 50

		ChangeFrame = vgui.Create("TacoPanel", HelpMenuFrameParent)
		ChangeLabel = vgui.Create("DHTML", ChangeFrame)

		ChangeFrame:SetPos(105, 25)
		ChangeFrame:SetSize(w, h)
		ChangeFrame:EnableTitle(false)
		ChangeFrame:EnableDragging(false)
		ChangeFrame:EnableCloseButton(false)
		ChangeFrame:SetColor(Color(30, 30, 30, 255))
		ChangeFrame:SetRoundness(4)

		ChangeLabel:SetPos(7, 5)
		ChangeLabel:SetSize(w - 14, h - 10)

		ChangeLabel:OpenURL("http://diaspora-community.com/viewforum.php?f=16")
	else
		ChangeFrame:SetVisible(true)
	end
end

function TutorialMenu()
	EaseUpFrameRate()

	if not IsValid(TutorialFrame) then
		local w,h = ScrW() - 210, ScrH() - 50

		TutorialFrame = vgui.Create("TacoPanel", HelpMenuFrameParent)
		TutorialLabel = vgui.Create("DHTML", TutorialFrame)

		TutorialFrame:SetPos(105, 25)
		TutorialFrame:SetSize(w, h)
		TutorialFrame:EnableTitle(false)
		TutorialFrame:EnableDragging(false)
		TutorialFrame:EnableCloseButton(false)
		TutorialFrame:SetColor(Color(30, 30, 30, 255))
		TutorialFrame:SetRoundness(4)

		TutorialLabel:SetPos(7, 5)
		TutorialLabel:SetSize(w - 14, h - 10)

		TutorialLabel:OpenURL("https://www.youtube.com/playlist?list=PLf3I7tC1fyryhZGA1FH-UlQz8s2pHDFvu")
	else
		TutorialFrame:SetVisible(true)
	end
end

function BugMenu()
	EaseUpFrameRate()

	if not IsValid(BugFrame) then
		local w,h = ScrW() - 210, ScrH() - 50

		BugFrame = vgui.Create("TacoPanel", HelpMenuFrameParent)
		BugLabel = vgui.Create("DHTML", BugFrame)

		BugFrame:SetPos(105, 25)
		BugFrame:SetSize(w, h)
		BugFrame:EnableTitle(false)
		BugFrame:EnableDragging(false)
		BugFrame:EnableCloseButton(false)
		BugFrame:SetColor(Color(30, 30, 30, 255))
		BugFrame:SetRoundness(4)

		BugLabel:SetPos(7, 5)
		BugLabel:SetSize(w - 14, h - 10)

		BugLabel:OpenURL("https://gitlab.ahbit.net/groups/diaspora/issues")
	else
		BugFrame:SetVisible(true)
	end
end

function WikiMenu()
	EaseUpFrameRate()

	if not IsValid(WikiFrame) then
		local w,h = ScrW() - 210, ScrH() - 50

		WikiFrame = vgui.Create("TacoPanel", HelpMenuFrameParent)
		WikiLabel = vgui.Create("DHTML", WikiFrame)

		WikiFrame:SetPos(105, 25)
		WikiFrame:SetSize(w, h)
		WikiFrame:EnableTitle(false)
		WikiFrame:EnableDragging(false)
		WikiFrame:EnableCloseButton(false)
		WikiFrame:SetColor(Color(30, 30, 30, 255))
		WikiFrame:SetRoundness(4)

		WikiLabel:SetPos(7, 5)
		WikiLabel:SetSize(w - 14, h - 10)

		WikiLabel:OpenURL("https://confluence.ahbit.net/display/dia/Home")
	else
		WikiFrame:SetVisible(true)
	end
end

function CommandsMenu()
	EaseUpFrameRate()

	if not IsValid(ComFrame) then
		local w,h = ScrW() - 210, ScrH() - 50

		ComFrame = vgui.Create("TacoPanel", HelpMenuFrameParent)
		ComLabel = vgui.Create("DLabel", ComFrame)

		ComFrame:SetPos(105, 25)
		ComFrame:SetSize(w, h)
		ComFrame:EnableTitle(false)
		ComFrame:EnableDragging(false)
		ComFrame:EnableCloseButton(false)
		ComFrame:SetColor(Color(30, 30, 30, 255))
		ComFrame:SetRoundness(4)

		ComLabel:SetPos(7, 10)
		ComLabel:SetSize(w - 14, math.min(table.Count(SBRP.commands) * 12, h - 20))
		local text = "The following commands are present in this gamemode currently: \n"

		for i,k in pairs(SBRP.commands) do
			text = text..k.name.." - "..k.help.."\n"
		end

		ComLabel:SetText(text)
	else
		ComFrame:SetVisible(true)
	end
end

function SteamMenu()
	EaseUpFrameRate()

	if not IsValid(SteamFrame) then
		local w,h = ScrW() - 210, ScrH() - 50

		SteamFrame = vgui.Create("TacoPanel", HelpMenuFrameParent)
		SteamLabel = vgui.Create("DHTML", SteamFrame)

		SteamFrame:SetPos(105, 25)
		SteamFrame:SetSize(w, h)
		SteamFrame:EnableTitle(false)
		SteamFrame:EnableDragging(false)
		SteamFrame:EnableCloseButton(false)
		SteamFrame:SetColor(Color(30, 30, 30, 255))
		SteamFrame:SetRoundness(4)

		SteamLabel:SetPos(7, 5)
		SteamLabel:SetSize(w - 14, h - 10)

		SteamLabel:OpenURL("http://steamcommunity.com/groups/SBDiaspora")
	else
		SteamFrame:SetVisible(true)
	end
end

function msgHelpMenu()
	if HelpMenuFrameParent then
		HelpMenuFrameParent:SetVisible(not visible)
		gui.EnableScreenClicker(not visible)
		visible = not visible
		return
	end

	visible = true

	local function closemain()
		HelpMenuFrameParent:SetVisible(false)
		gui.EnableScreenClicker(false)
	end

	HelpMenuFrameParent = vgui.Create("TacoPanel")

	gui.EnableScreenClicker(true)

	local w,h = ScrW() - 100, ScrH() - 100

	HelpMenuFrameParent:SetSize(w, h)
	HelpMenuFrameParent:Center()
	HelpMenuFrameParent:SetTitle("Diaspora Info Menu")
	HelpMenuFrameParent:SetVisible(true)
	HelpMenuFrameParent:SetPaintHook(HelpMenuPaint)
	HelpMenuFrameParent:SetCloseEvent(closemain)
	HelpMenuFrameParent:SetRoundness(4)
	HelpMenuFrameParent:SetColor(Color(20, 20, 20, 255))
    HelpMenuFrameParent:SetKeyboardInputEnabled(true)
	HelpMenuFrameParent:SetMouseInputEnabled(true)

	WikiMenu()
	ChangeMenu()
	TutorialMenu()
	BugMenu()
	CommandsMenu()
	SteamMenu()
	RulesMenu()
	InfoMenu()

	--Make the tab buttons
	local tab1 = HelpMenuFrameParent:AddButton2("Basic Info", 5, 15, 90, 30)
	tab1:SetColor(Color(30, 30, 30, 255))
	tab1:SetRoundness(4)
	tab1:SetCallback(InfoMenu)

	local tab2 = HelpMenuFrameParent:AddButton2("Rules", 5, 50, 90, 30)
	tab2:SetColor(Color(30, 30, 30, 255))
	tab2:SetRoundness(4)
	tab2:SetCallback(RulesMenu)

	local tab3 = HelpMenuFrameParent:AddButton2("Changelog", 5, 85, 90, 30)
	tab3:SetColor(Color(30, 30, 30, 255))
	tab3:SetRoundness(4)
	tab3:SetCallback(ChangeMenu)

	local tab4 = HelpMenuFrameParent:AddButton2("Tutorials", 5, 120, 90, 30)
	tab4:SetColor(Color(30, 30, 30, 255))
	tab4:SetRoundness(4)
	tab4:SetCallback(TutorialMenu)

	local tab5 = HelpMenuFrameParent:AddButton2("Wiki", 5, 155, 90, 30)
	tab5:SetColor(Color(30, 30, 30, 255))
	tab5:SetRoundness(4)
	tab5:SetCallback(WikiMenu)

	local tab6 = HelpMenuFrameParent:AddButton2("Bug Tracker", 5, 190, 90, 30)
	tab6:SetColor(Color(30, 30, 30, 255))
	tab6:SetRoundness(4)
	tab6:SetCallback(BugMenu)

	local tab7 = HelpMenuFrameParent:AddButton2("Commands", 5, 225, 90, 30)
	tab7:SetColor(Color(30, 30, 30, 255))
	tab7:SetRoundness(4)
	tab7:SetCallback(CommandsMenu)

	local tab8 = HelpMenuFrameParent:AddButton2("Steam Group", 5, 260, 90, 30)
	tab8:SetColor(Color(30, 30, 30, 255))
	tab8:SetRoundness(4)
	tab8:SetCallback(SteamMenu)
end
usermessage.Hook("ShowHelpMenu2", msgHelpMenu)
