AddCSLuaFile()

if not CLIENT then return end

local PANEL = {}
local GM = GM

surface.CreateFont("ContentDownloadFont", {
    font = "Consola Mono",
    size = SC2ScreenScale(46),
    weight = 1000,
    shadow = true,
    outline = true
})

-- Panel based blur function by Chessnut from NutScript
local blurmat = Material("pp/blurscreen")
local function blur(panel, layers, density, alpha)
    -- Its a scientifically proven fact that blur improves a script
    local x, y = panel:LocalToScreen(0, 0)
    surface.SetDrawColor(255, 255, 255, alpha)
    surface.SetMaterial(blurmat)

    for i = 1, 6 do
        blurmat:SetFloat("$blur", (i / layers) * density)
        blurmat:Recompute()

        render.UpdateScreenEffectTexture()
        surface.DrawTexturedRect(-x, -y, ScrW(), ScrH())
    end
end

function PANEL:Init()
    self:SetSize(ScrW(), ScrH())
    self:Center()
    self:SetBackgroundColor(Color(0, 0, 0, 10))
    gui.EnableScreenClicker(true)

    local OldPaint = self.Paint
    self.Paint = function(self, w, h)
        blur(self, 7, 15, 150)
        OldPaint(self, w, h)
    end

    self.TitleLabel = vgui.Create("DLabel", self)
	self.TitleLabel:SetFont("ContentDownloadFont")
	self.TitleLabel:SetText("Obtaining required content...")
    self.TitleLabel:SetContentAlignment(5)
	self.TitleLabel:Dock(FILL)

    -- This is normally a /terrible/ idea, buuut only one of these should ever exist so it's probably fine
    hook.Add("SC.Content.PostContentMounted", "CleanupContentProgress", function()
        self:Remove()

        -- HACK: Prevent nuclear meltdown from removing the hook
        timer.Simple(0, function()
            hook.Remove("SC.Content.PostContentMounted", "CleanupContentProgress")
        end)
    end)
end

vgui.Register("SC.ContentProgress", PANEL, "DPanel")