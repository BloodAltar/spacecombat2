--[[
	Space Combat Item Info Panel
	Author: Steve "Steeveeo" Green

	Displays in-depth information on a given item, i.e.
	stats, description, mass, etc.
]]--

local PANEL = {}

local GradientUpMat = Material( "gui/gradient_up" )

local TopPanelSize = 96
local TailBufferSize = 5

PANEL.IsCursorLocked = true

PANEL.ItemIcon = nil
PANEL.TitleLabel = nil
PANEL.RarityLabel = nil
PANEL.DescriptionBlock = nil

--Default Element Values
PANEL.IconPath = "models/props_interiors/pot01a.mdl"
PANEL.ItemName = "[PLACEHOLDER ITEM NAME]"
PANEL.ItemRarity = 1
PANEL.ItemDescription = "[PLACEHOLDER DESCRIPTION]"
PANEL.ItemArchetype = "Item"


--[[---------------------------------------------------------
   Initialize
-----------------------------------------------------------]]
function PANEL:Init()
	
	--Fixed width, auto-scaled height
	self:SetSize( 300, 0 )
	
	--Top Panel (so we can have the icon on the left and the main title on the right)
	local panelTop = vgui.Create( "DPanel", self )
	panelTop:SetPaintBackground( true )
	panelTop:SetBackgroundColor( Color(196, 196, 196, 255) )
	panelTop:SetSize( 0, TopPanelSize )
	panelTop:Dock( TOP )
	panelTop:DockMargin( 3, 3, 3, 0 )
	
	--Draw item rarity glow
	panelTop.Paint = function( x, y )
		local w, h = self:GetSize()
		
		--Backing
		surface.SetDrawColor( Color(40, 40, 40, 255) )
		surface.DrawRect( 0, 0, w, h )
		
		--Rarity Glow
		surface.SetDrawColor( GAMEMODE:GetItemRarityColor(self.ItemRarity) )
		
		surface.SetMaterial( GradientUpMat )
		surface.DrawTexturedRect( 0, 0, w, h )
	end
	
	--Item Preview
	local backPanel = vgui.Create( "DPanel", panelTop )
	backPanel:SetSize( TopPanelSize - 16, TopPanelSize - 16 )
	backPanel:Dock( LEFT )
	backPanel:DockMargin( 8, 8, 8, 8 )
	backPanel:SetPaintBackground( true )
	backPanel:SetBackgroundColor( Color(0, 0, 0, 255) )
	
	--Resource Icon (SpawnIcon for now, should probably be switched when I can figure out how to make a Material panel read from spawnicons/models/)
	--	Can also be switched for Material if someone feels like coming up with icons for each resource.
	self.ItemIcon = vgui.Create( "SpawnIcon", backPanel )
	self.ItemIcon:SetModel( "models/props_interiors/pot01a.mdl" )
	self.ItemIcon:Dock( FILL )
	self.ItemIcon:DockMargin( 8, 8, 8, 8 )
	
	
	--Top Panel - Title/Info
	local titlePanel = vgui.Create( "DPanel", panelTop )
	titlePanel:SetPaintBackground( false ) --Unsure if I want to remove this or not, check back later.
	titlePanel:Dock( FILL )
	
	--Title Label
	self.TitleLabel = vgui.Create( "DLabel", titlePanel )
	self.TitleLabel:SetFont( "DermaLarge" )
	self.TitleLabel:SetText( "Long Resource Title Here" )
	self.TitleLabel:SetWrap( true )
	self.TitleLabel:SetAutoStretchVertical( true )
	self.TitleLabel:Dock( TOP )
	self.TitleLabel:DockMargin( 5, 8, 5, 0 )
	
	
	--Rarity Info Label
	local rarityPanel = vgui.Create( "DPanel", self ) --Because the alignment functions sure don't do shit...
	rarityPanel:Dock( TOP )
	rarityPanel:SetPaintBackground( false )
	
	self.RarityLabel = vgui.Create( "DLabel", rarityPanel )
	self.RarityLabel:SetFont( "DermaDefaultBold" )
	self.RarityLabel:SetText( "[" .. string.upper(GAMEMODE:GetItemRarityName(self.ItemRarity)) .. " ITEM]")
	self.RarityLabel:SizeToContents()
	self.RarityLabel:SetColor( GAMEMODE:GetItemRarityColor(self.ItemRarity) )
	self.RarityLabel:Dock( RIGHT )
	self.RarityLabel:DockMargin( 3, 0, 3, 0 )
	
	
	--Description Text
	self.DescriptionBlock = vgui.Create( "RichText", self )
	self.DescriptionBlock:SetText( "" )
	self.DescriptionBlock:SetVerticalScrollbarEnabled( false )
	self.DescriptionBlock:Dock( TOP )
	self.DescriptionBlock:DockMargin( 5, 10, 5, 10 )
	
	
	--This panel intentionally left blank (buffer purposes)
	local bufferPanel = vgui.Create( "DPanel", self )
	bufferPanel:SetPaintBackground( false )
	bufferPanel:SetSize( 0, TailBufferSize )
	bufferPanel:Dock( TOP )
	
	
	--Size Panels to fit Contents
	self:InvalidateLayout( true )
end

function PANEL:PerformLayout( w, h )
	self.DescriptionBlock:SetToFullHeight()
	
	self:SizeToChildren( false, true )
end


--[[---------------------------------------------------------
   Accessors/Settings
-----------------------------------------------------------]]


function PANEL:SetItemIcon( modelPath )
	self.IconPath = modelPath
	self.ItemIcon:SetModel( self.IconPath )
end

function PANEL:GetItemIcon()
	return self.IconPath
end


function PANEL:SetItemName( name )
	self.ItemName = name

	self.TitleLabel:SetText( self.ItemName )
	self.TitleLabel:SizeToContents()
end

function PANEL:GetItemName()
	return self.ItemName
end


function PANEL:SetItemDescription( desc )
	self.ItemDescription = desc
	
	self.DescriptionBlock:SetText( self.ItemDescription )
	self:InvalidateLayout( false )
end

function PANEL:GetItemDescription()
	return self.ItemDescription
end


function PANEL:SetItemRarity( rarity )
	self.ItemRarity = rarity
	
	self.RarityLabel:SetColor( GAMEMODE:GetItemRarityColor(self.ItemRarity) )
	self.RarityLabel:SetText( string.upper("[" .. GAMEMODE:GetItemRarityName(self.ItemRarity) .. " " .. self.ItemArchetype .. "]") )
	self.RarityLabel:SizeToContents()
end

function PANEL:GetItemRarity()
	return self.Rarity
end


function PANEL:SetItemArchetype( archetype )
	self.ItemArchetype = archetype
	
	self.RarityLabel:SetText( string.upper("[" .. GAMEMODE:GetItemRarityName(self.ItemRarity) .. " " .. self.ItemArchetype .. "]") )
	self.RarityLabel:SizeToContents()
end

function PANEL:GetItemRarity()
	return self.Rarity
end


function PANEL:LoadFromResource( resource )
	local resourceInfo = GAMEMODE:GetResourceInfoTable( resource.Name )
	
	self:SetItemName( resource.Name )
	--self:SetAmount( resource.Amount )
	
	self:SetItemIcon( resourceInfo.Icon )
	self:SetItemRarity( resourceInfo.Rarity )
	self:SetItemArchetype( resourceInfo.Archetype )
	self:SetItemDescription( resourceInfo.Desc )
end


--[[---------------------------------------------------------
   Think
-----------------------------------------------------------]]
function PANEL:Think()
	if self.IsCursorLocked then
		--Move to Cursor and lock to Screen
		local x, y = gui.MousePos()
		local w, h = self:GetSize()
		
		x = math.Clamp( x, 0, surface.ScreenWidth() - w )
		y = math.Clamp( y, 0, surface.ScreenHeight() - h )
		
		self:SetPos( x, y )
	end
end


--[[---------------------------------------------------------
   Accessors
-----------------------------------------------------------]]
function PANEL:SetCursorLock( lock )
	self.IsCursorLocked = lock
end

function PANEL:GetCursorLock()
	return self.IsCursorLocked
end


vgui.Register( "InventoryInfoPanel", PANEL, "DPanel" )