local C = GM.LCS.class({
    Modifiers = {},
    Parent = nil,
    Mass = 1
})

function C:init()
    -- Does nothing by default, add functionality here
end

function C:PostComponentInit()
    -- Does nothing by default, this is called after the component is fully initialized.
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return false
end

-- If this returns true then the C:Draw function will be called. If it returns false, then it won't get called.
function C:ShouldDraw()
    return false
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return false
end

function C:Think()
    -- Does nothing by default, add functionality here
end

function C:Draw()
    -- Does nothing by default, add functionality here
end

function C:ReadNetworkUpdate()

end

function C:SendNetworkUpdate()

end

function C:GetMass()
    return self.Mass
end

function C:SetMass(NewMass)
    self.Mass = NewMass or 1
end

function C:GetParent()
    return self.Parent
end

function C:SetParent(NewParent)
    self.Parent = NewParent
end

function C:GetModifiers()
    return self.Modifiers
end

function C:SetModifiers(Modifiers)
    self.Modifiers = Modifiers
end

function C:SetModifier(Name, Value)
    self.Modifiers[Name] = Value
end

function C:GetModifier(Name)
    return self.Modifiers[Name] or 0
end

function C:OnProjectileEvent(Event, Info)
    -- Called when an event occurs. For example if the projectile is removed or another component raises an event.
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return false
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "ProjectileComponent"
end

function C:WriteCreationPacket()
    if not SERVER then return end

    net.WriteFloat(self.Mass)
end

function C:ReadCreationPacket()
    if not CLIENT then return end

    self.Mass = net.ReadFloat()
end

function C:Serialize()
    return {ComponentClass=self:GetComponentClass(), Modifiers=self:GetModifiers(), Mass=self.Mass}
end

function C:DeSerialize(Data)
    -- ComponentClass deserialization is handled by the Projectile class
    self:SetModifiers(Data.Modifiers or {})
    self:SetMass(Data.Mass or 1)
end

GM.class.registerClass("ProjectileComponent", C)