local GM = GM
local ProjectileRecipes = {}

GM.Projectiles = GM.Projectiles or {}
GM.Projectiles.Recipes = {}
GM.Projectiles.Recipes.LoadedRecipes = ProjectileRecipes

if SERVER then
    util.AddNetworkString("SC.ProjectileRecipesLoad")

    net.Receive("SC.ProjectileRecipesLoad", function(Length, Client)
        local ToSend = {}
        for Owner, Recipes in pairs(ProjectileRecipes) do
            if Owner == "NULL" or Owner == Client:SteamID() or Owner == team.GetName(Client:Team()) then
                for Name, Recipe in pairs(Recipes) do
                    table.insert(ToSend, Recipe)
                end
            end
        end

        net.Start("SC.ProjectileRecipesLoad")
        net.WriteUInt(table.Count(ToSend), 16)
        for _,Recipe in pairs(ToSend) do
            Recipe:WriteCreationPacket()
        end
        net.Send(Client)
    end)
else
    net.Receive("SC.ProjectileRecipesLoad", function()
        local NumberOfRecipes = net.ReadUInt(16)
        SC.Print("Got "..NumberOfRecipes.." serialized Projectile Recipes from the server!", 4)
        for I = 1, NumberOfRecipes do
            local Recipe = GM.class.new("ProjectileRecipe")
            local Valid = Recipe:ReadCreationPacket()
            if Valid then
                ProjectileRecipes[Recipe:GetOwnerID()] = ProjectileRecipes[Recipe:GetOwnerID()] or {}
                ProjectileRecipes[Recipe:GetOwnerID()][Recipe:GetName()] = Recipe

                SC.Print("Loaded Projectile Recipe "..Recipe:GetName(), 4)
            else
                SC.Error("Bad Projectile Recipe "..Recipe:GetName().." Owned By "..Recipe:GetOwnerID(), 4)
            end
        end
        SC.Print("Finished loading Projetile Recipes", 4)
        hook.Run("SC.ProjectileRecipesLoaded")
    end)
end

function GM.Projectiles.Recipes.ReloadProjectileRecipes()
    -- Server loads all files from disk for projectile types
    if SERVER then
        -- Get all the files
        local Files = file.Find(SC.DataFolder.."/projectiles/recipes/*", "DATA")

        -- For each file load the data into a new projectile type class
        for _,File in pairs(Files) do
            local NewProjectileRecipe = GM.class.new("ProjectileRecipe")
            if NewProjectileRecipe:LoadFromINI(File) then
                ProjectileRecipes[NewProjectileRecipe:GetOwnerID()] = ProjectileRecipes[NewProjectileRecipe:GetOwnerID()] or {}
                ProjectileRecipes[NewProjectileRecipe:GetOwnerID()][NewProjectileRecipe:GetName()] = NewProjectileRecipe
            else
                SC.Error("Failed to load projectile type from file "..File, 5)
            end
        end

        hook.Run("SC.ProjectileRecipesLoaded")

    -- Clients request data from the server for projectile types
    else
        -- Reset the existing data until we get the new stuff
        ProjectileRecipes = {}
        GM.Projectiles.Recipes.LoadedRecipes = ProjectileRecipes

        -- Request new data from the server
        net.Start("SC.ProjectileRecipesLoad")
        net.SendToServer()
    end
end

hook.Remove("SC.ProjectileTypesLoaded", "SC.LoadProjectileRecipes")
hook.Add("SC.ProjectileTypesLoaded", "SC.LoadProjectileRecipes", function()
    GM.Projectiles.Recipes.ReloadProjectileRecipes()
end)

local C = GM.LCS.class({
    -- Name of the projectile recipe
    Name = "Projectile",

    -- SteamID of the owner of the projectile recipe, or NULL if it wasn't created by a specific player (example: default projectile recipes)
    -- This can also be set to a faction name for projectiles that should be owned by a faction.
    OwnerID = "NULL",

    -- The basic type of projectile, used to validate the configuration. Determines how many of each type of component the recipe can have.
    ProjectileType = "None",

    -- A table of component information.
    -- Key is the name of the slot the component is in.
    -- Component information is a table containing the component name, configuration, and modifiers.
    Components = {},

    -- Used as an optimization so the resource cost isn't recalculated every time a gun fires
    HasCalculatedResourceCost = false,

    -- Table of resources needed to construct the projectile
    ResourceCost = {},
})

function C:init()
    -- Does nothing by default, add functionality here
end

function C:GetName()
    return self.Name
end

function C:GetOwnerID()
    return self.OwnerID
end

function C:GetProjectileType()
    return self.ProjectileType
end

function C:CalculateResourceCost()
    -- Todo: Loop over all components and add up their resource costs
end

function C:GetResourceCost()
    if not self.HasCalculatedResourceCost then
        self:CalculateResourceCost()
    end

    return self.ResourceCost
end

function C:LoadFromINI(FileName)
    local Success, Data = GM.util.LoadINIFile(SC.DataFolder.."/projectiles/recipes/"..FileName)

    -- Load all the data into the class if we got any data back
    if Success then
        self:DeSerialize(Data)
        return true
    end

    return false
end

function C:ValidateConfiguration()
    -- Todo: Load the data about the Projectile Type and make sure the components for the recipe are valid.
    -- If this returns false the recipe should be logged and removed immediately.

    return true
end

function C:GetSpawnData(Modifiers)
    if not self:ValidateConfiguration() then
        return false, {}
    end

    local SpawnData = {
		Position = Vector(0, 0, 0),
		Angles = Angle(0, 0, 0),
		Components = {},
		Modifiers = table.Copy(Modifiers or {})
    }

    local ProjectileType = GM.Projectiles.Types.LoadedTypes[self.ProjectileType]
    for Slot, Data in pairs(ProjectileType:GetMandatoryComponents()) do
        local ComponentData = GM.Projectiles.Components.LoadedComponents[Data.Component]

        if not ComponentData then
            SC.Error(string.format("Mandatory Slot %s Component %s does not exist!", Slot, Data.Component), 5)
            return false, {}
        end

        local Success, ConvertedData = ComponentData:GetComponentData(Data.Configuration or {}, SpawnData.Modifiers)
        if Success then
            table.insert(SpawnData.Components, ConvertedData)
        else
            SC.Error(string.format("Failed to create mandatory projectile component %s", Slot), 5)
            return false, {}
        end
    end

    for Slot, Data in pairs(self.Components) do
        local ComponentData = GM.Projectiles.Components.LoadedComponents[Data.Component]

        if not ComponentData then
            SC.Error(string.format("Slot %s Component %s does not exist!", Slot, Data.Component), 5)
            return false, {}
        end

        local Success, ConvertedData = ComponentData:GetComponentData(Data.Configuration or {}, SpawnData.Modifiers)
        if Success then
            table.insert(SpawnData.Components, ConvertedData)
        else
            SC.Error(string.format("Failed to create projectile component %s", Slot), 5)
            return false, {}
        end
    end

    return true, SpawnData
end

function C:CreateProjectileFromRecipe(Position, Angle, Owner, Modifiers)
    if not Position or not Angle or not IsValid(Owner) then
        return false
    end

    local Success, SpawnData = self:GetSpawnData(Modifiers)
    if not Success then
        return false
    end

    SpawnData.Position = Position
	SpawnData.Angles = Angle
	SpawnData.Owner = Owner

	local Projectile = GAMEMODE.class.new("Projectile")
    Projectile:DeSerialize(SpawnData)

    return true, Projectile
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteString(self.Name)
    net.WriteString(self.OwnerID)
    net.WriteString(self.ProjectileType)
    net.WriteTable(self.Components)
end

function C:ReadCreationPacket()
    if not CLIENT then return end
    self.Name = net.ReadString()
    self.OwnerID = net.ReadString()
    self.ProjectileType = net.ReadString()
    self.Components = net.ReadTable()

    self:CalculateResourceCost()

    if not self:ValidateConfiguration() then
        SC.Error("Tried to load invalid projectile recipe Name: "..tostring(self.Name).." OwnerID: "..tostring(self.OwnerID), 5)
        return false
    end

    return true
end

function C:Serialize()
    return {
        Configuration = {
            Name = self.Name,
            OwnerID = self.OwnerID,
            ProjectileType = self.ProjectileType
        },
        Components = self.Components
    }
end

function C:DeSerialize(Data)
    if Data.Configuration then
        self.Name = Data.Configuration.Name or "Projectile"
        self.OwnerID = Data.Configuration.OwnerID or "NULL"
        self.ProjectileType = Data.Configuration.ProjectileType or "None"
    end

    self.Components = Data.Components or {}

    self:CalculateResourceCost()

    if not self:ValidateConfiguration() then
        SC.Error("Tried to load invalid projectile recipe Name: "..tostring(self.Name).." OwnerID: "..tostring(self.OwnerID), 5)
    end
end

GM.class.registerClass("ProjectileRecipe", C)