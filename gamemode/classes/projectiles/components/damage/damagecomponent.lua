local C = GM.class.getClass("ProjectileComponent"):extends({
    Damage = {}
})

local BaseClass = C:getClass()

function C:init(Damage)
    self.Damage = Damage
    BaseClass.init(self)
end

function C:GetDamage()
    return table.Copy(self.Damage)
end

function C:SetDamage(NewDamage)
    self.Damage = NewDamage
end

function C:DealDamage(Target, HitData)
    if IsValid(Target) then
        Target:ApplyDamage(self:GetDamage(), self:GetParent():GetOwner(), self:GetParent():GetLauncher(), HitData)
    end
end

function C:OnProjectileEvent(Event, Info)
    if Event == "Collision" then
        self:DealDamage(Info.Entity, Info)
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return false
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return false
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)

    Data.Damage = self.Damage

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)

    self.Damage = Data.Damage or {}
end


-- This should always return the name of the class!
function C:GetComponentClass()
    return "DamageComponent"
end

GM.class.registerClass("DamageComponent", C)