local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({})
local Glow
local Shaft

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("PulseCannonEffectComponent")

if CLIENT then
    Glow = GM.MaterialFromVMT(
	    "StaffGlow",
	    [["UnLitGeneric"
	    {
	        "$basetexture"		"sprites/light_glow01"
	        "$nocull" 1
	        "$additive" 1
	        "$vertexalpha" 1
	        "$vertexcolor" 1
	    }]]
    )
    Shaft = Material("effects/ar2ground2");
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function C:Draw( )
    local Parent = self:GetParent()
	render.SetMaterial(Shaft)
	render.DrawBeam(Parent:GetPos(),Parent:GetPos() + (Parent:GetAngles():Forward() * -500 * self:GetScale()),200*self:GetScale(),1,0,self:GetColor())
	render.DrawBeam(Parent:GetPos(),Parent:GetPos() + (Parent:GetAngles():Forward() * -500 * self:GetScale()),75*self:GetScale(),1,0,self:GetColor())
	render.SetMaterial(Glow)
	render.DrawSprite(Parent:GetPos(),275*self:GetScale(),275*self:GetScale(),self:GetColor())
	render.DrawSprite(Parent:GetPos(),275*self:GetScale()/2,275*self:GetScale()/2,Color(255,255,255,255))
end

function C:GetComponentClass()
    return "PulseCannonEffectComponent"
end

GM.class.registerClass("PulseCannonEffectComponent", C)