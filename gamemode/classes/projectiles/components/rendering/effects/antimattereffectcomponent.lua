local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("AntimatterEffectComponent")

local Glow
local Shaft

if CLIENT then
    Glow = GM.MaterialFromVMT(
        "StaffGlow",
        [["UnLitGeneric"
        {
            "$basetexture"		"sprites/light_glow01"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
    )
    Shaft = Material("effects/ar2ground2");
end

function C:Draw( )
    local Parent = self:GetParent()
	local r, g, b, size = math.random(155)+100, math.random(155)+100, math.random(155)+100, math.random(150,200)*self:GetScale()
	render.SetMaterial(Shaft)
	render.DrawBeam(Parent:GetPos(),Parent:GetPos() + (Parent:GetAngles():Forward() * -1000 * self:GetScale()),(50+math.random(25)) * self:GetScale(),1,0,Color(62,g,b,150))
	render.DrawBeam(Parent:GetPos(),Parent:GetPos() + (Parent:GetAngles():Forward() * -1000 * self:GetScale()),(50+math.random(25)) * self:GetScale(),1,0,Color(r-25,0,0,255))
	render.SetMaterial(Glow)
	render.DrawSprite(Parent:GetPos(),size*(math.random(1)+1),size,Color(0,g,b,220))
	render.DrawSprite(Parent:GetPos(),size,size*(math.random(1)+1),Color(r,0,50,255))
	render.DrawSprite(Parent:GetPos(),size*3,size*3,Color(120,g,0,140))
end

function C:GetComponentClass()
    return "AntimatterEffectComponent"
end

GM.class.registerClass("AntimatterEffectComponent", C)