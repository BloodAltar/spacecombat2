--[[
/**********************************************
	Scalable Blaster Projectile Effect

	Author: Steeveeo
***********************************************/
--]]
local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    Velocity = -5000,
    CurTrailLength = 0,
    TrailLength = 750
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("ParticleBlasterEffectComponent")

local trailMat = Material("effects/ar2ground2")
local spriteFlareMat = Material("effects/flares/halo-flare_001")
local spriteCoreMat = Material("sprites/light_glow03")

function C:SetScale(NewScale)
    BaseClass.SetScale(self, NewScale)
    self.TrailLength = NewScale * 750
end

function C:Think()
	local Parent = self:GetParent()
	--Grow trail (so it doesn't shoot out the back of the gun)
	self.CurTrailLength = math.Clamp(self.CurTrailLength + (self.Velocity * FrameTime()), 0, self.TrailLength)

	--Particles
	for i = 1, 2 do
		local velocity = ((Parent:GetAngles():Forward() + (VectorRand() * 0.5)):GetNormalized() * math.Rand(200, 350)) + (Parent:GetAngles():Forward() * (self.Velocity * math.Rand(-0.1, 0.75)))

		local p = self:GetEmitter():Add("effects/flares/light-rays_001", Parent:GetPos())
		p:SetDieTime(math.Rand(0.5, 1))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(math.random(5, 10) * self:GetScale())
		p:SetEndSize(0)
	end

end

function C:Draw()
    local Parent = self:GetParent()
	render.SetMaterial(trailMat)
	render.DrawBeam(Parent:GetPos(), Parent:GetPos() + (Parent:GetAngles():Forward() * -self.CurTrailLength), 7.5 * self:GetScale(), 1, 0, Color(0, 144, 255, 255))

	render.SetMaterial(spriteFlareMat)
	render.DrawSprite(Parent:GetPos(), self:GetScale() * 200, self:GetScale() * 200, Color(200, 200, 255, 255))

	render.SetMaterial(spriteCoreMat)
	render.DrawSprite(Parent:GetPos(), self:GetScale() * 75, self:GetScale() * 75, Color(255, 255, 255, 255))

	return true
end

function C:GetComponentClass()
    return "ParticleBlasterEffectComponent"
end

GM.class.registerClass("ParticleBlasterEffectComponent", C)