AddCSLuaFile()

DEFINE_BASECLASS("base_lsentity")

ENT.PrintName = "Core Monitor"
ENT.Author = "Lt.Brandon"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "Monitor your core status."
ENT.Instructions = "Monitor your core status."
ENT.RenderGroup		= RENDERGROUP_TRANSLUCENT

ENT.Spawnable = false
ENT.AdminOnly = false

hook.Add("InitPostEntity", "sc_core_monitor_toolinit", function()
    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Core Monitor")
    Generator:SetClass("sc_core_monitor")
    Generator:SetDescription("Monitor your core status.")
    Generator:SetCategory("Utility")
    Generator:SetDefaultModel("models/sbep_community/d12shieldemitter.mdl")

    GAMEMODE:RegisterGenerator(Generator)
end)

function ENT:SetupGenerator()
end

local base = scripted_ents.Get("base_lsentity")
hook.Add( "InitPostEntity", "core_monitor_post_entity_init", function()
	base = scripted_ents.Get("base_lsentity")
end)

if CLIENT then
	local WorldTipFont_Small = "GModWorldtip_Smaller"
	local WorldTipFont_Big = "GModWorldtip_Bigger"

	surface.CreateFont( WorldTipFont_Small, {
		font		= "Helvetica",
		size		= SC2ScreenScale(18),
		weight		= 700
	})

	surface.CreateFont( WorldTipFont_Big, {
		font		= "Helvetica",
		size		= SC2ScreenScale(28),
		weight		= 900
	})

	surface.SetFont( WorldTipFont_Small )
	local width, char_height = surface.GetTextSize( "M" )
	local pie_column_width = 420
	local format = string.format
	local CoreFormatString =
[[Hull: %s/%s (%i%%)
Armor: %s/%s (%i%%)
Shield: %s/%s (%i%%)
Current Shield Recharge: %s/s
Capacitor: %s/%s (%i%%)]]

	function ENT:GetWorldTipBodySize()
		local h = 6 * (char_height) + 9 + 240
		local w = math.ceil(h / (ScrH() - 128))
		return math.max(w * pie_column_width - 18*2,420), math.min(h,ScrH()-128)
	end

	-------------------------
	-- colors
	-------------------------
	local colors = {
		Hull = Color(255, 0, 0, 255),
		Armor = Color(100, 100, 100, 255),
		Shield = Color(0, 0, 255, 255)
	}
	local white = Color(255,255,255,255)
	local black = Color(0,0,0,255)

	-------------------------
	-- DisplayModes
	-------------------------
	local deg2rad = math.pi / 180

	local function circleOutline( posx, posy, radius )
		surface.SetDrawColor( black )
		local lastx, lasty, firstx, firsty = 0,0,0,0
		local vertices = 360
		for i=1,vertices do
			local cos = math.cos(i/vertices*360*deg2rad)
			local sin = math.sin(i/vertices*360*deg2rad)
			local x = cos*radius + posx
			local y = sin*radius + posy

			if i > 1 then
				surface.DrawLine( lastx, lasty, x, y )
			else
				firstx, firsty = x, y
			end

			lastx, lasty = x, y
		end
		surface.DrawLine( lastx, lasty, firstx, firsty )
	end

	local function partialCircle( posx, posy, radius, start, stop )
		local poly = {x=posx,y=posy}
		local poly2 = {x=posx,y=posy}
		for i=start,stop do
			if i - start <= 180 then
				poly[#poly+1] = {
					x = math.cos( i * deg2rad ) * radius + posx,
					y = math.sin( i * deg2rad ) * radius + posy,
				}

				if i - start == 180 then
					poly2[#poly2+1] = {
						x = math.cos( i * deg2rad ) * radius + posx,
						y = math.sin( i * deg2rad ) * radius + posy,
					}
				end
			else
				poly2[#poly2+1] = {
					x = math.cos( i * deg2rad ) * radius + posx,
					y = math.sin( i * deg2rad ) * radius + posy,
				}
			end
		end
		poly[#poly+1] = {x=posx,y=posy}
		poly2[#poly2+1] = {x=posx,y=posy}
		surface.SetTexture(0)
		surface.DrawPoly( poly )
		if #poly2 > 2 then surface.DrawPoly( poly2 ) end
	end

	local function drawAllPartialCircles(data,pos,posx,posy,currentmax_idx)
		local currentmax = 0
		for i=2,#data do currentmax = currentmax + data[i][currentmax_idx] end

		local angoffset = 0
		for i=2,#data do
			local v = data[i]
			local name = v[1]
			local amount = v[2]

			if amount > 0 then
				local start = angoffset
				local stop = start + (amount / currentmax) * 360

				surface.SetDrawColor( colors[name] or white )
				partialCircle( posx, posy, 100, start, stop )

				surface.SetDrawColor( black )
				local x, y = posx + math.cos( start * deg2rad ) * 100, posy + math.sin( start * deg2rad ) * 100
				surface.DrawLine( x, y, posx, posy )

				angoffset = angoffset + (stop - start)
			end
		end
	end

	local function DrawCoreOverlay(Core, Pos) -- pie chart
		local Center = Pos.min.x + Pos.size.w / 2
		local Left = Center - 200
		local Right = Center + 120
		local data = {1, 	{"Hull", Core:GetHullAmount(), Core:GetHullMax()},
							{"Armor", Core:GetArmorAmount(), Core:GetArmorMax()},
							{"Shield", Core:GetShieldAmount(), Core:GetShieldMax()}}


		do
			draw.DrawText(format("%s - Status", Core:GetShipName()), WorldTipFont_Big, Left, Pos.min.y + Pos.edgesize, white, TEXT_ALIGN_LEFT)
			local ShieldRecharge = sc_ds(Core:CalculateRecharge(Core:GetShieldMax(), Core:GetShieldAmount(), Core:GetShieldRechargeTime()))
			local str = format(CoreFormatString,
					sc_ds(Core:GetHullAmount()), sc_ds(Core:GetHullMax()), Core:GetHullPercent()*100,
					sc_ds(Core:GetArmorAmount()), sc_ds(Core:GetArmorMax()), Core:GetArmorPercent()*100,
					sc_ds(Core:GetShieldAmount()), sc_ds(Core:GetShieldMax()), Core:GetShieldPercent()*100,
					ShieldRecharge,
					sc_ds(Core:GetCapAmount()), sc_ds(Core:GetCapMax()), Core:GetCapPercent()*100)


			draw.DrawText( str, WorldTipFont_Small, Left, Pos.min.y + Pos.edgesize + 40, white, TEXT_ALIGN_LEFT )
		end

		do
			local posx = Right
			local posy = Pos.min.y + Pos.edgesize + 130
			drawAllPartialCircles( data, Pos, posx, posy, 3 )
			circleOutline( Right, Pos.min.y + Pos.edgesize + 130, 100 )
		end
	end

	-------------------------
	-- Draw the stuff
	-------------------------
	function ENT:DrawWorldTipBody( WorldTip, pos )
		local Core = self:GetProtector()

		if not IsValid(Core) then
			local str = "Not linked"
			draw.DrawText( str, WorldTipFont_Small, pos.min.x + pos.size.w/2, pos.min.y + pos.edgesize, white, TEXT_ALIGN_CENTER )
			return
		end

		DrawCoreOverlay(Core, pos)
	end

	function ENT:Draw()
		self:DrawModel()

		if GPULib then
			if not self.GPU then self.GPU = GPULib.WireGPU( self, true ) end

			local max = self:OBBMaxs()
			local min = self:OBBMins()
			local sizex = (max.x - min.x)
			local sizey = (max.y - min.y)

			sizex = 512 * (1 + (sizex+18)/512)
			sizey = 512 * (1 + (sizey+200)/512)

			local size = math.max( sizex, sizey )

			self.GPU:RenderToWorld( size, size, function()
				GAMEMODE:PaintWorldTip( self )
			end, 1, true )
		end
	end

	return
end

duplicator.RegisterEntityClass("sc_core_monitor", GAMEMODE.MakeEnt, "Data")
