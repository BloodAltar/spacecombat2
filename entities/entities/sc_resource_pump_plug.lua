--[[
    SC Resource Pump Plug, spawned by the resource pump.

    Author: Hobnob
--]]

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_scplug"
ENT.PrintName = "SC Resource Plug"
ENT.Author = "Hobnob"
ENT.DisableDuplicator = true
ENT.Spawnable = false
ENT.AdminSpawnable = false

local base = scripted_ents.Get("base_scplug")
hook.Add("InitPostEntity", "base_resourcepump_plug_post_entity_init", function()
	base = scripted_ents.Get("base_scplug")

    if SERVER and AdvDupe then
        AdvDupe.AdminSettings.ChangeDisallowedClass("sc_resource_pump_plug", true, true)
    end
end)

if SERVER then
    function ENT:Initialize()
        self:SetModel("models/props_lab/tpplug.mdl")
        self.SC_Immune = true

        -- We need to disable some of the autoparenting functionality for these entities
        self.SCAutoParenter = {
            ["DisableConstraintRemoval"] = true,
            ["IgnoreSpecialConstraints"] = true,
            ["IgnoredConstraints"] = {"Elastic"}, -- This is only used if IgnoreSpecialConstraints is true. Fill with constraint types from the ConstraintTypes table
            ["DisableWeighting"] = true,
            ["DisableWelding"] = true,
            ["DisableParenting"] = true
        }

        base.Initialize(self)
    end

    -- Attempt to prevent people from doing things we don't want, while allowing things we do
    function ENT:CPPICanTool() return false end
    function ENT:CPPICanPhysgun() return true end
    function ENT:CPPICanPickup() return true end
    function ENT:CPPICanPunt() return true end
    function ENT:CanTool() return false end

    hook.Add("CanDrive", "CanDrivePumpPlugs", function(ply, ent) --No driving GS Pumps.
		if ent:GetClass() == "sc_resource_pump_plug" then
			return false
		end
	end)

	hook.Add("CanProperty", "CanPropertyPumpPlugs", function(ply, property, ent) --No using the context menu on GS Pumps either.
		if ent:GetClass() == "sc_resource_pump_plug" then
			return false
		end
	end)

    hook.Add("PhysgunDrop","PumpPlugFreezeLimit", function(ply,ent) --No taking the plug to a different planet and then just freezing it there
        if ent:GetClass() == "sc_resource_pump_plug" and ent:GetPos():Distance(ent:GetBase():GetPos()) > 1000 then
            local phys = ent:GetPhysicsObject()
            phys:EnableMotion(true)
            phys:Wake()
        end
    end)

    hook.Add("PhysgunPickup","PumpPlugAutoHomeReset", function(ply,ent) --Stops the plug being ripped from your hands
        if ent:GetClass() == "sc_resource_pump_plug" then
            timer.Destroy("PumpPlugAutoHome"..ent:EntIndex())
        end
    end)

    -- Traces don't like parented entities, let's try this instead.
    function ENT:PhysicsCollide(data)
        if self:IsValidSocket(data.HitEntity) and self:GetPos():Distance(self:GetBase():GetPos()) <= 800 and self:CanConnect() then
            --Changing collision rules within a callback is likely to cause crashes!
            local ent = data.HitEntity
            timer.Simple(0.1, function() if ent:IsValid() and self:IsValid() then self:ConnectToSocket(ent, false) end end)
        end
    end

    -- Gets the trace direction used in FindSocket
    function ENT:GetTraceDirection()
        return -self:GetForward()
    end

    -- Gets the plug socket position offset
    function ENT:GetOffset()
        return Vector(5, 13, 10.2)
    end

    -- What we want to connect to
    function ENT:IsValidSocket(Ent)
        -- The Pump that spawned us.
        local BaseEnt = self:GetBase()

        if  IsValid(BaseEnt) and
            BaseEnt.IsPump and
            BaseEnt:IsLinked() and
            IsValid(Ent) and
            Ent.IsPump and
            Ent:IsLinked() then
            return true
        else
            return false
        end
    end

    -- How to connect to the socket
    function ENT:ConnectToSocket(Ent, Force)
        if not (self:IsValidSocket(Ent) and self:CanConnect()) and not Force then return false end
        if Ent.Plugged then return false end

        self:GetBase():ConnectToPump(Ent)
        self:SetConnected(true)
        self:SetPos(Ent:LocalToWorld(self:GetOffset()))
        self:SetAngles(Ent:GetAngles())
        self:GetPhysicsObject():EnableMotion(false)
        self:ForcePlayerDrop()
        self:SetMoveType(MOVETYPE_NONE)
        self:SetParent(Ent)

        if NADMOD then
            NADMOD.SetOwnerWorld(self)
        end

        self.ConnectedSocket = Ent
        self.PhysgunDisabled = true
        Ent.Plugged = true

        return true
    end

    -- How to disconnect from the socket
    function ENT:DisconnectFromSocket(Force)
        if self:IsConnected() or Force then
            if IsValid(self:GetConnectedSocket()) then
			    self:GetConnectedSocket():EmitSound("physics/metal/metal_computer_impact_bullet" .. math.random(1, 3) .. ".wav", 500)
                self:GetConnectedSocket().Plugged = false
            end

            self:SetParent(nil)
            if IsValid(self:GetBase()) and IsValid(self:GetBase():GetOutboundPump()) then
				self:GetBase():Disconnect()
			end

            if self:GetConnectedSocket() ~= self:GetBase() then --doesnt auto reconnect if disconnected from base pump, so you can actually use it :D
                timer.Create("PumpPlugAutoHome" .. self:EntIndex(), 5, 1, function()
                    if IsValid(self) and not self:IsConnected() and IsValid(self:GetBase()) then
                        self:ConnectToSocket(self:GetBase())
                    end
                end)
            end

            self:EmitSound("physics/metal/metal_computer_impact_bullet" .. math.random(1, 3) .. ".wav", 500)
            self.ConnectedSocket = nil
            self:SetConnected(false)
            self:AddConnectionDelay(2)
            if self.CPPISetOwnerless then
                self:CPPISetOwnerless(true)
            end
            self.PhysgunDisabled = false
			self:SetMoveType(MOVETYPE_VPHYSICS)
			self:GetPhysicsObject():EnableMotion(true)
			self:GetPhysicsObject():Wake()

			timer.Simple(0, function() --0 = next frame
				if not IsValid(self) then return end --Not even sure how anyone managed to pull that off, but it happened
				self:SetPos(self:LocalToWorld(Vector(10, 0, 0)))
			end)
        end
    end

    -- Think Function, run every second
    function ENT:Think()
        if not IsValid(self:GetBase()) then
			self:Remove() --We shouldn't exist without a socket to connect to
			return
        end

        if not IsValid(self:GetBase().PlugConstraint) then
            self:Remove()
            return
        end

        local Distance = self:GetPos():Distance(self:GetBase():GetPos())

        if self:IsConnected() then
            if Distance >= 800 then
                -- We're about to break, start creaking like a broken chair.
                self:EmitSound("ambient/misc/metal_str" .. math.random(1, 3) .. ".wav", 500)

                if Distance >= 1000 then
					local core = self:GetBase():GetProtector()
					local otherCore = self:GetConnectedSocket():GetProtector()

                    -- Disconnect from the socket, explosively.
                    self:DisconnectFromSocket()

                    -- Rip and Tear!
                    local ExplosionScale = Distance / 1000
					if core and IsValid(core) then core:ApplyDamage({WTF=(100000 * ExplosionScale)}, self, self, {Entity=core}) end
					if otherCore and IsValid(otherCore) then otherCore:ApplyDamage({WTF=(100000 * ExplosionScale)}, self, self, {Entity=core}) end

                    local effectdata = {}
					    effectdata.Start = self:GetPos()
					    effectdata.Origin = self:GetPos()
					    effectdata.Normal = self:GetTraceDirection()
					    effectdata.Magnitude = math.random() + 2 / 2 + 0.25
					    effectdata.Scale = 0.5

					-- TODO: New Effect? Sparks plz.
				    SC.CreateEffect("impact_artillery", effectdata)
                end
                self:Remove()
                return
            end
        else
            if Distance <= 800 and self:CanConnect() then
                self:FindSocket()
            end
        end

        self:NextThink(CurTime() + 1)

        return true
    end
end