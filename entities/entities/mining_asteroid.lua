--[[
	Minable Asteroid Ent for Diaspora Mining

	Author: Steeveeo
]]--
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_mineableentity"

ENT.PrintName = "Asteroid"
ENT.Author = "Steeveeo"
ENT.Contact = "http://diaspora-community.com"
ENT.Purpose = "I am an asteroid, chalk full of stuff!"
ENT.Instructions = ""

ENT.Spawnable = false
ENT.AdminSpawnable = true

local base = scripted_ents.Get("base_mineableentity")
hook.Add( "InitPostEntity", "mining_asteroid_post_entity_init", function()
	base = scripted_ents.Get("base_mineableentity")
end)

if SERVER then
	--Runs after MiningSequence ends
	function ENT:OnMined(MiningSpeed)
		local Return = base.OnMined(self, MiningSpeed)

		if not self.IsDead then
			if self:IsEmpty() then
				self:SetColor(Color(255, 255, 255, 0))

				local effect = {}
				effect.Origin = self:GetPos()
				effect.Entity = self
				SC.CreateEffect("mining_asteroiddeath", effect)

				self.IsDead = true
				timer.Simple(1, function()
					self:Remove()
				end)
			end
		end

		return Return
	end
end