AddCSLuaFile()

DEFINE_BASECLASS("base_moduleentity")

ENT.PrintName = "Targeting Device"
ENT.Author = "Lt.Brandon"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "Ordering weapons around since 3702!"
ENT.Instructions = "1. Find nearest target. 2. Put weight on fire button. 3. ??? 4. Salvage wreckage."

ENT.Spawnable = false
ENT.AdminOnly = false
ENT.IsSCTargeter = true

--[[
    Format:
    {
        DisplayName = "Name",
        Classes = {
            "entity_class"
        },
        OnlyProtected = false,
        IgnoreOwnedEntities = false,
        ShipClasses = {
            "Fighter"
        },
        CheckFaction = true
    }
]]

local DefaultTargetFilters = {
    {
        DisplayName = "Combat",
        Classes = {
            "ship_core"
        },
        CheckFaction = true,
        IgnoreOwnedEntities = true
    },
    {
        DisplayName = "Salvaging",
        Classes = {
            "sc_wreck"
        },
        CheckFaction = false
    },
    {
        DisplayName = "Mining",
        Classes = {
            "mining_mineral",
            "mining_mineral_parent",
            "mining_rock",
            "mining_asteroid"
        },
        CheckFaction = false
    }
}

local base = scripted_ents.Get("base_moduleentity")
hook.Add("InitPostEntity", "sc_targeter_post_entity_init", function()
	base = scripted_ents.Get("base_moduleentity")
end)

function ENT:SharedInit()
   base.SharedInit(self)

   SC.NWAccessors.CreateNWAccessor(self, "Target", "entity", NULL)
   SC.NWAccessors.CreateNWAccessor(self, "TargetList", "table", {})
   SC.NWAccessors.CreateNWAccessor(self, "TargetOverride", "bool", false)
   SC.NWAccessors.CreateNWAccessor(self, "LinkedPod", "entity", NULL)

   self:SetModuleName("Targeting Device")
   self:SetCycleDuration(5)
end

function ENT:IsLinkedToPod()
    return IsValid(self:GetLinkedPod())
end

function ENT:GetTargetPosition()
    local Target = self:GetTarget()
    if IsValid(Target) then
        return Target:GetPos()
    end

    return vector_origin
end

function ENT:GetPredictedTargetPosition(TimeToImpact)

end

if CLIENT then
	return
end

function ENT:UnlinkPod()
    if self:IsLinkedToPod() then
        self:SetLinkedPod(NULL)
    end
end

function ENT:LinkPod(NewPod)
    if self:IsLinkedToPod() then
        self:UnlinkPod()
    end

    if IsValid(NewPod) and NewPod:IsVehicle() then
        self:SetLinkedPod(NewPod)
        return true
    else
        return false
    end
end

function ENT:Initialize()
    base.Initialize(self)

    self.TargetFilters = DefaultTargetFilters

    self.TargetID = 0
    self.FilterID = 1

    self.FireGroups = {}

    -- Input hooks
    self.KeysDown = {}
    hook.Add("PlayerButtonDown", "TargeterKeyPress_"..self:EntIndex(), function(ply, key)
        if not IsValid(self) or not self:IsLinkedToPod() then return end
        if ply:GetVehicle() == self:GetLinkedPod() then
            self.KeysDown[key] = true

            self:OnKeyDown(ply, key)
        end
    end)

    hook.Add("PlayerButtonUp", "TargeterKeyPress_"..self:EntIndex(), function(ply, key)
        if not IsValid(self) or not self:IsLinkedToPod() then return end
        if ply:GetVehicle() == self:GetLinkedPod() then
            self.KeysDown[key] = nil

            self:OnKeyUp(ply, key)
        end
    end)
end

function ENT:OnRemove()
    base.OnRemove(self)
    hook.Remove("PlayerButtonDown", "TargeterKeyPress_"..self:EntIndex())
    hook.Remove("PlayerButtonUp", "TargeterKeyPress_"..self:EntIndex())
end

function ENT:SetFireGroupFiring(FireGroup, Firing)
    self.FireGroups[FireGroup] = Firing
end

function ENT:IsFireGroupFiring(FireGroup)
    return self.FireGroups[FireGroup] or false
end

function ENT:CanTargetEntity(Entity)
    return true
end

local function GetEntityFaction(Entity)
    local Owner = SC.GetEntityOwner(Entity)
    if IsValid(Owner) then
        return Owner:Team()
    end

    return -1
end

function ENT:FindAllTargetsInArea(FilterData, Position, Radius)
    local Radius2 = Radius * Radius
    local ShipClasses = FilterData.ShipClasses
    local OnlyProtected = FilterData.OnlyProtected
    local IgnoreOwned = FilterData.IgnoreOwnedEntities
    local MyOwner = SC.GetEntityOwner(self)
    local MyFaction = GetEntityFaction(self)
    local Targets = {}
    for _, Class in pairs(FilterData.Classes) do
        local Found = ents.FindByClass(Class)
        for __, Ent in ipairs(Found) do
            if self:CanTargetEntity(Ent) then
                if Ent:GetPos():DistToSqr(Position) <= Radius2 then
                    if not OnlyProtected or Ent:IsProtected() then
                        if not FilterData.CheckFaction or (GetEntityFaction(Ent) ~= MyFaction) then
                            if not IgnoreOwned or (SC.GetEntityOwner(Ent) ~= MyOwner) then
                                if ShipClasses then
                                    if Ent:IsProtected() then
                                        if table.HasValue(ShipClasses, Ent:GetProtector():GetShipClass()) then
                                            table.insert(Targets, Ent)
                                        end
                                    end
                                else
                                    table.insert(Targets, Ent)
                                end
                            end
                        end
                    end
                end
            end
        end
    end

    return Targets
end

function ENT:UpdatePredictedTargetPosition()

end

function ENT:CycleTarget(Forward)
    if Forward then
        local NextID, NextTarget
        if self.TargetID == 0 then
            NextID, NextTarget = next(self:GetTargetList())
        else
            NextID, NextTarget = next(self:GetTargetList(), self.TargetID)
        end

        if IsValid(NextTarget) then
            self:SetTarget(NextTarget)
            self.TargetID = NextID
        else
            NextID, NextTarget = next(self:GetTargetList())
            self:SetTarget(NextTarget or NULL)
            self.TargetID = NextID or 0
        end
    else
        -- TODO: Implement previous selection
    end

    if WireLib then
        WireLib.TriggerOutput(self, "Target", self:GetTarget())
    end
end

function ENT:CycleFilter(Forward)
    if Forward then
        local NextID, NextFilter = next(self.TargetFilters, self.FilterID)
        if NextFilter then
            self.FilterID = NextID
        else
            self.FilterID = 1
        end
    else
        -- TODO: Implement previous selection
    end

    self.TargetID = 0
    self:SetTarget(NULL)

    if WireLib then
        WireLib.TriggerOutput(self, "Target", self:GetTarget())
    end

    self:UpdateTargetList()
end

function ENT:HandleVisualTargetSelection(Ply)
    local TargetList = self:GetTargetList()
    local BestTarget
    local BestTargetDot = -1
    local PlayerPos = Ply:EyePos()
    local EyeAngles = Ply:EyeAngles():Forward()
    for _, ToCheck in ipairs(TargetList) do
        if IsValid(ToCheck) then
            local TargetDot = (ToCheck:GetPos() - PlayerPos):GetNormalized():Dot(EyeAngles)
            if TargetDot > BestTargetDot then
                BestTarget = ToCheck
                BestTargetDot = TargetDot
            end
        end
    end

    if BestTargetDot > 0 and IsValid(BestTarget) then
        self:SetTarget(BestTarget)
    else
        self:SetTarget(NULL)
    end

    if WireLib then
        WireLib.TriggerOutput(self, "Target", self:GetTarget())
    end
end

function ENT:OnKeyDown(Ply, Key)
    if Key == Ply.TargeterKeys["NextTarget"] then
        self:CycleTarget(true)
    elseif Key == Ply.TargeterKeys["PrevTarget"] then
        self:CycleTarget(false)
    elseif Key == Ply.TargeterKeys["NextFilter"] then
        self:CycleFilter(true)
    elseif Key == Ply.TargeterKeys["PrevFilter"] then
        self:CycleFilter(false)
    elseif Key == Ply.TargeterKeys["SelectTarg"] then
        self:HandleVisualTargetSelection(Ply)
    elseif Key == Ply.WeaponKeys["Primary"] then
        self:SetFireGroupFiring("Primary", true)
    elseif Key == Ply.WeaponKeys["Secondary"] then
        self:SetFireGroupFiring("Secondary", true)
    elseif Key == Ply.WeaponKeys["Tertiary"] then
        self:SetFireGroupFiring("Tertiary", true)
    end
end

function ENT:OnKeyUp(Ply, Key)
    if Key == Ply.WeaponKeys["Primary"] then
        self:SetFireGroupFiring("Primary", false)
    elseif Key == Ply.WeaponKeys["Secondary"] then
        self:SetFireGroupFiring("Secondary", false)
    elseif Key == Ply.WeaponKeys["Tertiary"] then
        self:SetFireGroupFiring("Tertiary", false)
    end
end

function ENT:OnModuleEnabled()
    if not self:IsProtected() then SC.Error("sc_module_targeter::OnModuleEnabled Core doesn't exist yet? WTF?", 5) return end

    self:GetProtector():SetTargeter(self)
end

function ENT:OnModuleDisabled()
    if not self:IsProtected() then SC.Error("sc_module_targeter::OnModuleDisabled Core doesn't exist? WTF?", 5) return end

    self:GetProtector():SetTargeter(NULL)

    self:SetTarget(NULL)
    self:SetTargetList({})

    self.TargetID = 0

    if WireLib then
        WireLib.TriggerOutput(self, "Target", self:GetTarget())
    end
end

function ENT:UpdateTargetList()
    local OldTarget = self:GetTarget()

    -- TODO: Add range modifiers
    local Targets = self:FindAllTargetsInArea(self.TargetFilters[self.FilterID], self:GetPos(), 32000)

    if IsValid(OldTarget) then
        if self:CanTargetEntity(OldTarget) then
            local NewID = 0
            for ID, Entity in ipairs(Targets) do
                if Entity == OldTarget then
                    NewID = ID
                    break
                end
            end

            if NewID > 0 then
                self.TargetID = NewID
            else
                self.TargetID = 0
                self:SetTarget(NULL)
            end
        end
    end

    self:SetTargetList(Targets)
end

function ENT:OnCycleStarted()
    self:UpdateTargetList()

    if WireLib then
        WireLib.TriggerOutput(self, "Target", self:GetTarget())
    end
end

function ENT:OnStoppedCycling()
    -- Called when the entity stops cycling. This is different from OnCycledFinished, which calls at the end of every cycle.
    self:SetTarget(NULL)
    self:SetTargetList({})

    self.TargetID = 0

    if WireLib then
        WireLib.TriggerOutput(self, "Target", self:GetTarget())
    end
end

function ENT:OnCycleUpdated(TimeRemaining)
    local CurrentTarget = self:GetTarget()
    if IsValid(CurrentTarget) then
        if self:CanTargetEntity(CurrentTarget) then
            self:UpdatePredictedTargetPosition()
        else
            -- Can not target this entity anymore, reset target and ID
            -- TODO: Might be a good idea to remove it from the target list? Might also just be a waste of CPU time?
            self:SetTarget(NULL)
            self.TargetID = 0
        end
    else
        -- Target no longer exists, reset ID
        self.TargetID = 0
    end
end

function ENT:Think()
    base.Think(self)

    -- This entity needs to think more frequently than a standard module for target prediction
    self:NextThink(CurTime() + 0.2)
    return true
end

function ENT:GetWirePorts()
    return {
            "On",
            "Mute",
            "Hide overlay",
            "Target Override [ENTITY]"
        },
        {
            "Cycle Percent",
            "On",
            "Target [ENTITY]"
        }
end

function ENT:SaveSCInfo()
    return {
        TargetFilters = self.TargetFilters
    }
end

function ENT:LoadSCInfo(Info)
    self.TargetFilters = Info.TargetFilters or table.Copy(DefaultTargetFilters)
end

duplicator.RegisterEntityClass("sc_module_targeter", GAMEMODE.MakeEnt, "Data")
