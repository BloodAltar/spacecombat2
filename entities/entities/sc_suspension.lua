AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_scentity"
ENT.PrintName	= "Vehicle Suspension"
ENT.Author	= "Lt.Brandon"

ENT.Spawnable	= true
ENT.AdminSpawnable = false
ENT.IsDriveLineComponent = true
ENT.IsMechanical = true

-- Suspension
ENT.SuspensionLength = 17.5
ENT.SpringWeightRating = 2200
ENT.Stiffness = 10--8.85
ENT.Damping = 1.8--2.45
ENT.Multiplier = 1
ENT.SpringPosition = 0
ENT.PreviousSpringPosition = 0
ENT.PreviousCompresison = 0
ENT.SuspensionForward = Vector(1, 0, 0)
ENT.SuspensionRight = Vector(0, 1, 0)
ENT.Compression = 0

-- Wheel
ENT.WheelModel = "models/sprops/trans/wheel_c/wheel45.mdl"--"models/sprops/trans/wheel_c/wheel45.mdl"
ENT.WheelScale = Vector(1, 1, 1)
ENT.WheelOffset = Vector(0, 0, 0)
ENT.WheelRadius = 22.5
ENT.WheelWidth = 8
ENT.WheelWeight = 25
ENT.WheelSlideFrictionCoefficient = 0.5
ENT.WheelRollingResistanceCoefficient = 0.375
ENT.WheelRPM = 0
ENT.MaxSidewaysForce = 2.5
ENT.MinCompressionGrip = 0.95
ENT.MaxCompressionGrip = 1.15
ENT.VehicleWeight = 1500
ENT.NumberOfWheels = 4
ENT.WheelInertia = 0
ENT.PeakTraction = 1.05
ENT.PeakSlipRatio = 6
ENT.SlipFalloff = 0.6
ENT.SlipFalloffRatio = 20

-- Steering
ENT.HasSteering = true
ENT.SteeringAngle = 35
ENT.SteeringFrictionMultiplier = 1.2
ENT.SteeringSensitivity = 1
ENT.CurrentSteeringAngle = 0
ENT.SteeringInput = 0

-- Brakes
ENT.HasBrakes = true
ENT.BrakeTorque = 1600 / 0.14
ENT.BrakePressure = 0

ENT.DriveTorque = 0
ENT.DriveRPM = 0
ENT.DriveEnergy = 0
ENT.AppliedDriveEnergy = 0

function ENT:CreateHologram(model, pos, ang, col, par, scale, relativetoself)
    if CLIENT then
        local h = ClientsideModel(model, (col and col.a or 255) == 255 and RENDERMODE_NORMAL or RENDERMODE_TRANSALPHA)
        h:SetPos((relativetoself and self or par):LocalToWorld(pos))
        h:SetAngles((relativetoself and self or par):LocalToWorldAngles(ang))
        h:SetModelScale(scale or Vector(1, 1, 1))
        h:SetColor(col or Color(255, 255, 255, 255))
        --h:SetParent(par)
        local Min, Max = h:GetModelRenderBounds()
        h:SetRenderBounds(Min, Max, Vector(100, 100, 100))

        table.insert(self.Holograms, h)

        return h
    else
        local h = ents.Create("info_target")
        h:SetPos((relativetoself and self or par):LocalToWorld(pos))
        h:SetAngles((relativetoself and self or par):LocalToWorldAngles(ang))
        h:SetParent(par)

        function h:UpdateTransmitState()
	        return TRANSMIT_NEVER
        end

        h:Spawn()

        table.insert(self.Holograms, h)

        return h
    end
end

function ENT:CleanupHolograms()
    for i,k in pairs(self.Holograms) do
        if IsValid(k) then
            k:Remove()
        end
    end
end

function ENT:Initialize()
    self:SetModel("models/maxofs2d/cube_tool.mdl")

    self.BaseClass.Initialize(self)

    self.Holograms = {}

    self.WheelInertia = self.WheelWeight * math.pow(self.WheelRadius, 2) / 2
    self.Wheel = self:CreateHologram(self.WheelModel, Vector(0, 0, 0), Angle(0, 0, 0), Color(255, 255, 255, 255), self, self.WheelScale, true)

    if CLIENT then return end

    if WireLib then
        self.Inputs = WireLib.CreateSpecialInputs(self, {"Steering", "Brake"}, {"NORMAL", "NORMAL"})
        self.Outputs = WireLib.CreateOutputs(self, {"DEBUG [STRING]"})
    end
end

function ENT:SharedInit()
    self.BaseClass.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "WheelRotation", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "CurrentSteeringAngle", "number", 0)
end

function ENT:OnReloaded()
    self.WheelInertia = self.WheelWeight * math.pow(self.WheelRadius, 2) / 2
end

function ENT:OnRemove()
    self:CleanupHolograms()
end

local function MakeFromXZ(X, Z)
    local NewZ = Z:GetNormalized()
    local NewX = X:GetNormalized()

    if (NewZ:Dot(NewX) > 0.9999) then
        NewX = (math.abs(NewZ.Z) < 0.9999) and Vector(0, 0, 1) or Vector(1, 0, 0)
    end

    NewY = NewZ:Cross(NewX):GetNormalized()
    NewX = NewY:Cross(NewZ)

    return NewX, NewY, NewZ
end

function ENT:ApplySuspensionForce(Parent, TraceStart, TraceEnd, TraceResult)
    local Phys = Parent:GetPhysicsObject()
    if TraceResult.Hit then
        local Compression = 1 - TraceResult.Fraction
        self.Compression = Compression

        self.SpringPosition = (TraceStart - TraceResult.HitPos):Length()
        local DamperVelocity = (self.SpringPosition - self.PreviousSpringPosition) / FrameTime()
        self.PreviousSpringPosition = self.SpringPosition

        local SpringForce = (Compression * self.Stiffness * 100) - (self.Damping * DamperVelocity)

        if SERVER and IsValid(Phys) then
            Phys:ApplyForceOffset(TraceResult.HitNormal * SpringForce * self.SpringWeightRating * self.Multiplier * FrameTime(), self:GetPos())
        end
    else
        self.SpringPosition = self.SuspensionLength + self.WheelRadius
		self.PreviousPosition = self.SpringPosition
		self.Compression = 0.0
    end

    -- Apply gravity forces on the wheel
    if SERVER and IsValid(Phys) then
        Phys:ApplyForceOffset(-vector_up * (9.81 * self.WheelWeight) * Parent:GetGravity(), self.Wheel:GetPos())
    end
end

function ENT:UpdateSteering(Parent, TraceStart, TraceEnd, TraceResult)
    if self.HasSteering then
        local OldSteeringAngle = self:GetCurrentSteeringAngle()
        self:SetCurrentSteeringAngle(math.Clamp(Lerp(self.SteeringSensitivity * FrameTime(), OldSteeringAngle, self.SteeringInput * self.SteeringAngle), -self.SteeringAngle, self.SteeringAngle))

        local ForwardAngle = self:GetForward():AngleEx(self:GetUp())
        ForwardAngle:RotateAroundAxis(self:GetUp(), self:GetCurrentSteeringAngle())

        self.SuspensionForward = ForwardAngle:Forward()
        self.SuspensionRight = ForwardAngle:Right()
    else
        self.SuspensionForward = self:GetForward()
        self.SuspensionRight = self:GetRight()
    end
end

function ENT:CalculateSidewaysForce(Parent, SurfaceFriction, CompressionGripRatio)
    local Phys = Parent:GetPhysicsObject()
    local Velocity = Phys:GetVelocity() * 2.54

    -- Velocity in cm/s
    local VelocityAtSuspension = self:GetPhysicsObject():GetVelocityAtPoint(self:LocalToWorld(-Vector(0, 0, self.SpringPosition + self.WheelRadius))) * 2.54
    local MaxSidewaysForce = self.MaxSidewaysForce * 1000
    local Dot = VelocityAtSuspension:Dot(self.SuspensionRight)
    local SidewaysForce = Dot * self.WheelSlideFrictionCoefficient * CompressionGripRatio * SurfaceFriction * (self.VehicleWeight / self.NumberOfWheels / self.WheelRadius)
    SidewaysForce = math.Clamp(SidewaysForce, -MaxSidewaysForce, MaxSidewaysForce)

    if self.HasSteering then
        SidewaysForce = SidewaysForce * Lerp(math.abs(self:GetCurrentSteeringAngle() / self.SteeringAngle), 1, self.SteeringFrictionMultiplier)
    end

    return SidewaysForce
end

function ENT:CalculateBrakeTorque(CurrentWheelInertia)
    if not self.HasBrakes then
        return 0
    end

    if self.BrakePressure > 0 then
        local BrakeDirection = self.WheelRPM > 0 and -1 or 1
        return math.Clamp(BrakeDirection * self.BrakeTorque * self.BrakePressure, -CurrentWheelInertia, CurrentWheelInertia)
    end

    return 0
end

function ENT:CalculateForwardForce(Parent, RollingFriction, SurfaceFriction, CompressionGripRatio)
    local ParentPhys = self:GetPhysicsObject()

    local TotalTorque = 0
    if math.abs(self.DriveRPM) > 0 then
        local RPMDiff = math.Clamp((self.DriveRPM - self.WheelRPM) / 2.5, -1, 1)
        TotalTorque = self.DriveTorque * RPMDiff
    end

    if self.Compression > 0.0001 then
        local CarVelocity = ParentPhys:WorldToLocalVector(ParentPhys:GetVelocityAtPoint(self:LocalToWorld(-Vector(0, 0, self.SpringPosition + self.WheelRadius))))
        if math.abs(TotalTorque) > 0.5 then
            self.WheelRPM = ((self.WheelRPM * 5) + ((CarVelocity.x / self.WheelRadius) * 9.55)) / 6
        else
            self.WheelRPM = (self.WheelRPM + ((CarVelocity.x / self.WheelRadius) * 9.55)) / 2
        end
    elseif math.abs(TotalTorque) < 0.5 then
        -- lazy af
        self.WheelRPM = Lerp(0.01, self.WheelRPM, 0)
    end

    local CurrentWheelInertia = math.abs(self.WheelInertia * self.WheelRPM)
    local BrakeTorque = self:CalculateBrakeTorque(CurrentWheelInertia)
    TotalTorque = TotalTorque + BrakeTorque

    local WheelLoad = TotalTorque / self.WheelRadius
    local WheelAngularVelocity = self.WheelRPM / 9.55

    WheelAngularVelocity = WheelAngularVelocity + (TotalTorque / self.WheelInertia)

    local ForwardForce
    if self.Compression > 0.0001 then
        if math.abs(self.DriveRPM) > 0 then
            local CarVelocity = ParentPhys:WorldToLocalVector(ParentPhys:GetVelocity())
            local SlipRatio = math.Clamp(((WheelAngularVelocity * self.WheelRadius) - CarVelocity.x) / CarVelocity.x, -self.SlipFalloffRatio, self.SlipFalloffRatio)
            ForwardForce = (math.Clamp(SlipRatio, -self.PeakSlipRatio, self.PeakSlipRatio) / self.PeakSlipRatio) * self.PeakTraction

            if math.abs(SlipRatio) > self.PeakSlipRatio then
                ForwardForce = ForwardForce * (1 - (math.abs(SlipRatio / self.SlipFalloffRatio) * (1 - self.SlipFalloff)))
            end
        else
            ForwardForce = self.WheelRPM > 0 and 1 or -1
        end

    else
        ForwardForce = 0
    end

    local FrictionForce = RollingFriction * SurfaceFriction * CompressionGripRatio
    local FrictionTorque = FrictionForce * self.WheelRadius * self.WheelRPM > 0 and 1 or -1

    ForwardForce = ForwardForce * (FrictionForce + math.abs(WheelLoad))

    WheelAngularVelocity = WheelAngularVelocity + (((ForwardForce / self.WheelRadius)) / self.WheelInertia)

    self.WheelRPM = WheelAngularVelocity * 9.55


    return WheelLoad
end

function ENT:ApplyFrictionForces(Parent, TraceStart, TraceEnd, TraceResult)
    local Phys = Parent:GetPhysicsObject()
    if IsValid(Phys) then
        -- Get info about the current surface
        local SurfaceFriction
        if self.Compression > 0.0001 then
            local SurfaceData = util.GetSurfaceData(TraceResult.SurfaceProps)
            if SurfaceData then
                SurfaceFriction = math.Clamp(SurfaceData.friction, 0.9, 1.2)
            else
                SurfaceFriction = 1.0
            end
        else
            SurfaceFriction = 0
        end

        -- Calculate the rolling friction
        local RollingFriction = self.WheelRollingResistanceCoefficient * self.VehicleWeight / self.NumberOfWheels / self.WheelRadius
        local CompressionGripRatio = Lerp(1 - TraceResult.Fraction, self.MinCompressionGrip, self.MaxCompressionGrip)

        -- Calculate wheel forces
        local SidewaysForce = self:CalculateSidewaysForce(Parent, SurfaceFriction, CompressionGripRatio)
        local ForwardForce = self:CalculateForwardForce(Parent, RollingFriction, SurfaceFriction, CompressionGripRatio)

        if self.Compression < 0.0001 then
            return
        end

        -- Apply the wheel forces
        local SurfaceForward, SurfaceRight = self.SuspensionForward, -self.SuspensionRight--MakeFromXZ(self.SuspensionForward, TraceResult.HitNormal) --self.SuspensionForward, self.SuspensionRight
        local RightForce = SurfaceRight * SidewaysForce
        local ForwardForce = SurfaceForward * ForwardForce
        Phys:ApplyForceOffset((ForwardForce + RightForce) * (self.VehicleWeight / self.NumberOfWheels) * FrameTime(), TraceResult.HitPos)
    end
end

function ENT:UpdateWheelPositionAndRotation(Parent, TraceStart, TraceEnd, TraceResult)
    local NewPos = self:LocalToWorld(-Vector(0, 0, self.SpringPosition - self.WheelRadius))
    self.Wheel:SetPos(NewPos)

    self:SetWheelRotation((self:GetWheelRotation() + self.WheelRPM * 6 * FrameTime()) % 360)
    local NewAngle = self.SuspensionForward:AngleEx(self:GetUp())
    NewAngle:RotateAroundAxis(self.SuspensionRight, 360 - self:GetWheelRotation())
    self.Wheel:SetAngles(NewAngle)
end

function ENT:Think()
    local Parent = self:GetParent()
    if not IsValid(Parent) then
        return
    end

    -- Find out where the ground is
    local TraceStart = self:GetPos()
    local TraceEnd = TraceStart + (-self:GetUp() * (self.SuspensionLength + self.WheelRadius))
    local TraceData = {
        start = TraceStart,
        endpos = TraceEnd,
        filter = {self, Parent}
    }

    local TraceResult = util.TraceLine(TraceData)

    -- This shouldn't normally happen, but the wiki says it can
    if not TraceResult then
        return
    end

    -- Update the steering
    self:UpdateSteering(Parent, TraceStart, TraceEnd, TraceResult)

    if CLIENT then
        -- Update suspension position for client
        self:ApplySuspensionForce(Parent, TraceStart, TraceEnd, TraceResult)
    else
        if self:GetPhysicsObject():IsAsleep() then
            -- Hack to force updates even if physics object is sleeping
            self:PhysicsUpdate()
        end
    end

    -- Update wheel visuals
    self:UpdateWheelPositionAndRotation(Parent, TraceStart, TraceEnd, TraceResult)

    self:NextThink(CurTime())
    return true
end

function ENT:PhysicsUpdate()
    local Parent = self:GetParent()
    if not IsValid(Parent) then
        return
    end

    -- Find out where the ground is
    local TraceStart = self:GetPos()
    local TraceEnd = TraceStart + (-self:GetUp() * (self.SuspensionLength + self.WheelRadius))
    local TraceData = {
        start = TraceStart,
        endpos = TraceEnd,
        filter = {self, Parent}
    }

    local TraceResult = util.TraceLine(TraceData)

    -- This shouldn't normally happen, but the wiki says it can
    if not TraceResult then
        return
    end

    -- Apply the suspension forces
    self:ApplySuspensionForce(Parent, TraceStart, TraceEnd, TraceResult)

    -- Apply the friction forces (static friction, grip, etc.)
    self:ApplyFrictionForces(Parent, TraceStart, TraceEnd, TraceResult)
end

function ENT:OnEngineUpdate(UpdateTime, InputRPM, InputEnergy, InputTorque)
    local Parent = self:GetParent()
    if not IsValid(Parent) then
        return
    end

    self.DriveTorque = InputTorque / 0.14
    self.DriveRPM = InputRPM
    self.DriveEnergy = InputEnergy

    local RPMDiff = self.DriveRPM - self.WheelRPM
    local AngularVelocity = RPMDiff / 9.55
    local EnergyDifference = ((0.5 * self.WheelInertia * math.pow(AngularVelocity, 2)) / self.DriveTorque) * (RPMDiff > 0 and 1 or -1)

    WireLib.TriggerOutput(self, "DEBUG", Format("%d", EnergyDifference))

    return EnergyDifference
end

function ENT:TriggerInput(Name, Value)
    if Name == "Steering" then
        self.SteeringInput = math.Clamp(-Value, -1, 1)
    elseif Name == "Brake" then
        self.BrakePressure = math.Clamp(Value, 0, 1)
    end
end

function ENT:UpdateOutputs()
    if WireLib then

    end
end