AddCSLuaFile()

DEFINE_BASECLASS("base_lsentity")

ENT.PrintName = "Suit Dispenser"
ENT.Author = "Lt.Brandon, Divran, and Steeveeo"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "A giant vacuum set to reverse."
ENT.Instructions = "Flip switch, get pelted with life support cans."

ENT.Spawnable = false
ENT.AdminOnly = false

local function InitGeneratorInfo()
    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Suit Dispenser")
    Generator:SetClass("sc_dispenser")
    Generator:SetDescription("Flip switch, get pelted with life support cans.")
    Generator:SetCategory("Utility")
    Generator:SetDefaultModel("models/slyfo_2/acc_food_fooddispenser.mdl")

    GAMEMODE:RegisterGenerator(Generator)
end
hook.Remove("InitPostEntity", "sc_dispenser_toolinit")
hook.Add("InitPostEntity", "sc_dispenser_toolinit", InitGeneratorInfo)
hook.Remove("OnReloaded", "sc_dispenser_post_reloaded")
hook.Add("OnReloaded", "sc_dispenser_post_reloaded", InitGeneratorInfo)

function ENT:SetupGenerator()
end

function ENT:Initialize()
    BaseClass.Initialize(self)

    if CLIENT then
        self:SetOverlayText(string.format("Press [%s] to refill suit", string.upper(input.LookupBinding("+use"))))
    end
end

if CLIENT then
	return
end

function ENT:Setup()
	self:SetUseType( SIMPLE_USE )
	self.cooldown = 0
	self.local_canisters = 0

	self.disableProdution = false
	self.ownerOnly = false

	self:SetupWirePorts()
    self:UpdateOutputs()
end


------------------------------------------------------------ LOCAL STORAGE

--------------------------------------------------
-- Wire stuff
--------------------------------------------------
function ENT:GetWirePorts()
	local inputs = {}
	local outputs = {}

	inputs[#inputs+1] = "Owner Only"

	return inputs, outputs
end

function ENT:TriggerInput( name, value )
    if name == "Owner Only" then
		self.ownerOnly = (value ~= 0)
	end
end

function ENT:Use( ply )
	if ply:GetShootPos():Distance( self:GetPos() ) > 256 then return end
	if self.cooldown > CurTime() then return end

	self.cooldown = CurTime() + 1

	--Owner Lock
	if self.ownerOnly and self.CPPICanTool and ply ~= self:CPPIGetOwner() then
		self:EmitSound( "buttons/weapon_cant_buy.wav" )
		return
    end

    local Storage = ply:GetStorage()
    local AirName = "Oxygen"
    local CoolantName = "Water"
    local NeedsAir = Storage:GetAmount(AirName) < Storage:GetMaxAmount(AirName)
    local NeedsCoolant = Storage:GetAmount(CoolantName) < Storage:GetMaxAmount(CoolantName)
    local NeedsEnergy = Storage:GetAmount("Energy") < Storage:GetMaxAmount("Energy")
    local Success = NeedsAir or NeedsCoolant or NeedsEnergy

    if NeedsAir then
        local NeededAir = Storage:GetMaxAmount(AirName) - Storage:GetAmount(AirName)
        if self:ConsumeResource(AirName, NeededAir) then
            if not Storage:SupplyResource(AirName, NeededAir) then
                self:SupplyResource(AirName, NeededAir)
                Success = false
            end
        else
            Success = false
        end
    end

    if NeedsCoolant then
        local NeededCoolant = Storage:GetMaxAmount(CoolantName) - Storage:GetAmount(CoolantName)
        if self:ConsumeResource(CoolantName, NeededCoolant) then
            if not Storage:SupplyResource(CoolantName, NeededCoolant) then
                self:SupplyResource(CoolantName, NeededCoolant)
                Success = false
            end
        else
            Success = false
        end
    end

    if NeedsEnergy then
        local NeededEnergy = Storage:GetMaxAmount("Energy") - Storage:GetAmount("Energy")
        if self:ConsumeResource("Energy", NeededEnergy) then
            if not Storage:SupplyResource("Energy", NeededEnergy) then
                self:SupplyResource("Energy", NeededEnergy)
                Success = false
            end
        else
            Success = false
        end
    end

	if Success then
		self:EmitSound( "buttons/lever5.wav" )
	else
		self:EmitSound( "buttons/weapon_cant_buy.wav" )
	end
end

duplicator.RegisterEntityClass("sc_dispenser", GAMEMODE.MakeEnt, "Data")
