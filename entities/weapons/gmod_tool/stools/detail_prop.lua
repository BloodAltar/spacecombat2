//================================
// DETAIL PROP REPLACEMENT TOOL
// 		Author: Steeveeo
//================================

TOOL.Category		= "Construction"
TOOL.Name			= "#Detail Prop"
TOOL.Command		= nil
TOOL.ConfigName		= ""

cleanup.Register( "detail_props" )

if (CLIENT) then

	language.Add("Tool.detail_prop.name", "Detail Prop Tool")
	language.Add("Tool.detail_prop.desc", "Left click: Change Prop into Detail")
	language.Add("Tool.detail_prop.0", "Made by: Steeveeo")
	
	language.Add("Undone_Detail Prop", "Undone Detail Prop")
	language.Add("Undone_Revert Detail Prop", "Undone Reverted Detail Prop")
	
	language.Add("Cleanup_detail_props", "Detail Props")
	language.Add("Cleaned_detail_props", "Cleaned up all Detail Props")
	language.Add("SBoxLimit_detail_prop", "You've reached the Detail Prop limit!")

end

local function copyConstraints(from, to)
	local constraints = constraint.FindConstraints(from, "Weld")
	for k,v in pairs(constraints) do
		constraint.Weld(to, v.Entity[1].Entity, 0, 0)
	end
end

CreateConVar('sbox_maxdetail_prop', 500)
	
local function MakeDetailProp( pl, model, ang, pos )

	local DProp = ents.Create( "detail_prop" )
	if ( !DProp:IsValid() ) then return end
	 
	DProp:SetModel(model)
	DProp:SetAngles(ang)
	DProp:SetPos(pos)
	DProp:Spawn()
	
	DProp:SetPlayer( pl )
	DProp.Owner = pl
	
	pl:AddCount( "detail_props", DProp )
	
	DoPropSpawnedEffect( DProp )
	
	return DProp
	
end

local function SetMatAndColor(ply, ent, data)
	ent:SetColor(data.color)
	ent:SetMaterial(data.material)
	ent:SetSkin(data.skin)
end

//---------------------------------------------------------
// Replaces the hit prop with a Detail Prop replacement
//---------------------------------------------------------
function TOOL:LeftClick( trace )
	
	if( SERVER ) then
	
		local ply = self:GetOwner()
		
		--We can only change prop_physics into detail_prop.
		--I don't think it would be possible to do anything else
		--without screwing things over.
		if ( !trace.Entity or trace.Entity:GetClass() ~= "prop_physics" ) then
			return false
		end
		
		--Get Target Ent Info
		local ent = trace.Entity
		local model = ent:GetModel()
		local ang = ent:GetAngles()
		local pos = ent:GetPos()
		local color = ent:GetColor()
		local material = ent:GetMaterial()
		local skin = ent:GetSkin()
		
		--Create Copy
		local DProp = MakeDetailProp( ply, model, ang, pos )
		local data = {}
		data.skin = skin
		data.material = material
		data.color = color
		SetMatAndColor(ply, DProp, data)
		
		--Copy Constraints
		copyConstraints(ent, DProp)
		
		--Remove Target
		ent:Remove()
		
		undo.Create("Detail Prop")
			undo.AddEntity(DProp)
			
			undo.AddFunction( function(u)
				local Prop = ents.Create("prop_physics")
				local Ent = u.Entities[1]
				
				if !IsValid(Ent) then return end
				
				local undoPly = u.Owner
				if ( !Prop:IsValid() ) then return end
				Prop:SetModel(Ent:GetModel())
				Prop:SetAngles(Ent:GetAngles())
				Prop:SetPos(Ent:GetPos())
				Prop:SetColor(Ent:GetColor())
				Prop:SetMaterial(Ent:GetMaterial())
				Prop:SetSkin(Ent:GetSkin())
				Prop:Spawn()
				
				local phys = Prop:GetPhysicsObject()
				if (phys:IsValid()) then
					phys:Wake()
					phys:EnableMotion(false)
				end
				
				--Copy constraints back
				copyConstraints(Prop, ent)
		
				--Prop:SetPlayer( ply )
				Prop.Owner = ply
				
				undo.Create("Prop")
					undo.AddEntity(Prop)
					undo.SetPlayer(undoPly)
				undo.Finish()
			end)
			
			undo.SetPlayer(ply)
		undo.Finish()
		
		ply:AddCleanup( "detail_props", DProp )
	
	end
	
	return true
end

//---------------------------------------------------------
// Replaces the hit Detail Prop with a Physics Prop
//---------------------------------------------------------
function TOOL:RightClick( trace )
	
	if( SERVER ) then
	
		local ply = self:GetOwner()
		
		--We can only change prop_physics into detail_prop.
		--I don't think it would be possible to do anything else
		--without screwing things over.
		if ( !trace.Entity or trace.Entity:GetClass() ~= "detail_prop" ) then
			return false
		end
		
		--Get Target Ent Info
		local ent = trace.Entity
		local model = ent:GetModel()
		local ang = ent:GetAngles()
		local pos = ent:GetPos()
		local color = ent:GetColor()
		local material = ent:GetMaterial()
		local skin = ent:GetSkin()
		
		--Create Copy
		local Prop = ents.Create("prop_physics")
		
		if ( !Prop:IsValid() ) then return end
		Prop:SetModel(model)
		Prop:SetAngles(ang)
		Prop:SetPos(pos)
		Prop:Spawn()
		
		local phys = Prop:GetPhysicsObject()
		if (phys:IsValid()) then
			phys:Wake()
			phys:EnableMotion(false)
		end
		
		local data = {}
		data.skin = skin
		data.material = material
		data.color = color
		SetMatAndColor(ply, Prop, data)
		
		--Copy Constraints
		copyConstraints(ent, Prop)
		
		--Remove Target
		ent:Remove()
		
		undo.Create("Revert Detail Prop")
			undo.AddEntity(Prop)
			
			undo.AddFunction( function(u)
				local Ent = u.Entities[1]
				
				--Copy Constraints
				copyConstraints(ent, DProp)
				
				if !IsValid(Ent) then return end
				
				local undoPly = u.Owner
				local model = Ent:GetModel()
				local ang = Ent:GetAngles()
				local pos = Ent:GetPos()
				local data = {}
				data.skin = skin
				data.material = material
				data.color = color
				
				local DProp = MakeDetailProp( undoPly, model, ang, pos )
				SetMatAndColor(ply, DProp, data)
				
				local phys = DProp:GetPhysicsObject()
				if (phys:IsValid()) then
					phys:Wake()
					phys:EnableMotion(false)
				end
				
				--Copy constraints back
				copyConstraints(Ent, DProp)
		
				--Prop:SetPlayer( ply )
				Prop.Owner = ply
				
				undo.Create("Detail Prop")
					undo.AddEntity(DProp)
					undo.SetPlayer(undoPly)
				undo.Finish()
			end)
			
			undo.SetPlayer(ply)
		undo.Finish()
		
		ply:AddCleanup( "props", Prop )
	
	end
	
	return true
end


function TOOL.BuildCPanel( CPanel )

	//Header
	CPanel:AddControl( "Header", { Text = "#Tool.detail_prop.name", Description	= "#Tool.detail_prop.desc" }  )
	
	//Disable Detail Prop Rendering
	CPanel:AddControl("Checkbox", { Label = "Draw Detail Props", Command = "detailprop_enable", Description = "Toggles the rendering of Detail Props"  } )
	
	//Toggle Shadows
	CPanel:AddControl("Checkbox", { Label = "Detail Prop Shadows", Command = "detailprop_drawshadow", Description = "Toggles the rendering of shadows on Detail Props"  } )
	
	//Prop View Distance
	CPanel:AddControl( "Slider",  { Label	= "View Distance",
					Type	= "Float",
					Min		= 0,
					Max		= 10000,
					Command = "detailprop_viewdistance",
					Description = "Distance until Detail Props stop rendering."})
					
	//Shadow Distance Percent
	CPanel:AddControl( "Slider",  { Label	= "Shadow Start",
					Type	= "Float",
					Min		= 0,
					Max		= 1,
					Command = "detailprop_shadowstart",
					Description = "Percent of View Distance when shadows stop rendering."})
	
end
