SWEP.PrintName			= "Plasma SMG" -- This will be shown in the spawn menu, and in the weapon selection menu
SWEP.Author			= "Brandon Garvin" -- These two options will be shown when you have the weapon highlighted in the weapon selection menu
SWEP.Instructions		= "Summon Satan with the power of plasma!"

SWEP.Spawnable = true
SWEP.AdminOnly = true

-- Add the ammo type
game.AddAmmoType({
    name = "ammo_plasma",
	dmgtype = DMG_BULLET,
	tracer = TRACER_LINE,
	plydmg = 50,
	npcdmg = 50,
	force = 200,
	minsplash = 10,
	maxsplash = 5
})

SWEP.Primary.ClipSize		= 254
SWEP.Primary.DefaultClip	= 9000
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo		    = "ammo_plasma"

SWEP.Slot			    = 1
SWEP.SlotPos			= 2
SWEP.DrawAmmo			= true
SWEP.DrawCrosshair		= true

SWEP.ViewModel			= "models/weapons/c_smg1.mdl"
SWEP.WorldModel			= "models/weapons/w_smg1.mdl"
SWEP.UseHands           = true
SWEP.HoldType           = "smg"
SWEP.FireSound          = Sound("spacecombat2/weapons/small_laser_01.wav")--Sound("sb/railgunlight.wav")
SWEP.FireDelay          = 0.12
SWEP.FireSound2         = Sound("spacecombat2/weapons/med_laser_02.wav")--Sound("sb/railgunlight.wav")
SWEP.FireDelay2         = 0.4

function SWEP:Initialize()
	self:SetWeaponHoldType(self.HoldType)
end

function SWEP:SpawnProjectile(Type, Data, SendUpdates)
    if CLIENT then return end

    local Position = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
    local Angle = self.Owner:GetAimVector():Angle()
    local Success, Projectile = GAMEMODE.Projectiles.Recipes.LoadedRecipes["NULL"][Type]:CreateProjectileFromRecipe(Position, Angle, self.Owner)

    Projectile.ShouldSendNetUpdates = SendUpdates or false

    local MovementComponent = Projectile:GetMovementComponent()

    if not MovementComponent then
        MovementComponent = GAMEMODE.class.new("MovementComponent")
        Projectile:AddComponent(MovementComponent)
    end

    --MovementComponent:SetVelocity(self.Owner:GetVelocity())
    MovementComponent:AddRelativeVelocity(Vector(8500, 0, 0))

    Projectile:Spawn()

    return Projectile
end

function SWEP:PrimaryAttack()
    if self:CanPrimaryAttack() then
        self.Owner:DoAttackEvent()
        self.Weapon:SetNextPrimaryFire(CurTime() + self.FireDelay)

        if CLIENT then return end
        self.FiredProjectiles = self.FiredProjectiles or {}
        local Projectile = self:SpawnProjectile("Tiny Kinetic Cannon Round")

        table.insert(self.FiredProjectiles, Projectile)
        self:TakePrimaryAmmo(1)
        self.Owner:EmitSound(self.FireSound, 80, 100, 1)
    end
end

function SWEP:SecondaryAttack()
    if self:CanSecondaryAttack() then
        self.Owner:DoAttackEvent()
        self.Weapon:SetNextSecondaryFire(CurTime() + self.FireDelay2)

        if CLIENT then return end
        self.FiredProjectiles = self.FiredProjectiles or {}
        local Projectile = self:SpawnProjectile("Small Explosive Missile", {})
        table.insert(self.FiredProjectiles, Projectile)
        Projectile:FireNetworkedEvent("SetTargetEntity", {Target=self.Owner:GetEyeTrace().Entity})
        self:TakeSecondaryAmmo(1)
        self.Owner:EmitSound(self.FireSound2, 80, 100, 1)
    end
end