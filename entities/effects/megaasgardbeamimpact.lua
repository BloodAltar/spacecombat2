local bMats = {}
bMats.Glow1 = GAMEMODE.MaterialFromVMT(
	"sc_blue_ball01",
	[["UnLitGeneric"
	{
		"$basetexture"		"sprites/bluelight1"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)


bMats.Glow2 = GAMEMODE.MaterialFromVMT(
	"sc_blue_ball02",
	[["UnLitGeneric"
	{
		"$basetexture"		"effects/blueflare1"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)
bMats.Glow3 = GAMEMODE.MaterialFromVMT(
	"sc_blue_ball03",
	[["UnLitGeneric"
	{
		"$basetexture"		"effects/blueflare1"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)
local lMats = {}

lMats.Glow2 = GAMEMODE.MaterialFromVMT(
	"sc_blue_beam02",
	[["UnLitGeneric"
	{
		"$basetexture"		"sprites/laserbeam"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)

lMats.Glow3 = GAMEMODE.MaterialFromVMT(
	"sc_blue_beam03",
	[["UnLitGeneric"
	{
		"$basetexture"		"sprites/laserbeam"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)



--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )

	self.StartPos 	= data:GetStart()	
	self.EndPos 	= data:GetOrigin()
	self.Multi 		= data:GetMagnitude( )
	self.rad 		= 16
	self.MultiBeam	= data:GetScale()
	self.TimeLeft = CurTime() + 3
	self.Fade = 1
	
	self:SetRenderBoundsWS( self.StartPos, self.EndPos )
	
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
  	local timeleft = self.TimeLeft - CurTime()
	if timeleft > 0 then 
		local ftime = FrameTime()
		self.Fade = (timeleft / 3)
		
		return true
	else
		return false	
	end
		return false	
end


--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	
	
   	render.SetMaterial( bMats.Glow1 )		
   	render.DrawBeam( self.EndPos, self.StartPos, 300, (1)*((2*CurTime())-(0.0005*self.MultiBeam)), (1)*(2*CurTime()), Color( 255, 0, 0, 255  * self.Fade) )
   	
	render.SetMaterial( bMats.Glow2 )
   	render.DrawSprite(self.StartPos, 800 , 800 , Color(122, 27, 20, 255 * self.Fade)) 
	
	render.SetMaterial( bMats.Glow3 )
   	render.DrawSprite(self.StartPos, 400 , 400 , Color(255, 255, 255, 255 * self.Fade)) 
	
	render.SetMaterial( bMats.Glow2 )
   	render.DrawSprite(self.EndPos, 2000 , 2000 , Color(122, 27, 20, 255 * self.Fade)) 
	
	render.SetMaterial( bMats.Glow3 )
   	render.DrawSprite(self.EndPos, 1000 , 1000 , Color(255, 255, 255, 255 * self.Fade)) 
   	
	render.SetMaterial( lMats.Glow2 )	
   	render.DrawBeam( self.EndPos , self.StartPos , 188, (1)*((2*CurTime())-(0.001*self.MultiBeam)) , (1)*(2*CurTime()), Color( 122, 27, 20, 255 * self.Fade) )
   	
	render.SetMaterial( lMats.Glow3 )
   	render.DrawBeam( self.EndPos , self.StartPos, 70, (1)*((2*CurTime())-(0.001*self.MultiBeam)) , (1)*(2*CurTime()) , Color( 255, 255, 255, 255 * self.Fade ) )
	
	render.SetMaterial( lMats.Glow3 )	
   	render.DrawBeam( self.EndPos , self.StartPos, 128, (1)*((2*CurTime())-(0.001*self.MultiBeam)) , (1)*(2*CurTime()), Color( 255, 255, 255, 155 * self.Fade) )
	return false
					 
end
