--[[*********************************************
	Scalable Autocannon Hit Effect
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal() or Vector(0,0,0)
	self.Scale = data:GetScale()
	
	self.Emitter = ParticleEmitter( self.Pos )
	self.GroundEmitter = ParticleEmitter( self.Pos, true )
	
	--Sparks
	for i=1, 5 do
		local velocity = (self.Normal + (VectorRand() * 0.85)):GetNormalized() * (math.Rand(150, 500) * self.Scale)
		
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.15, 0.25))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartSize(3 * self.Scale)
		p:SetEndSize(0)
		p:SetStartLength(50 * self.Scale)
		p:SetEndLength(5 * self.Scale)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetColor(255, 220, 200)
	end
	
	--Flare
	local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
	p:SetDieTime(math.Clamp(math.Rand(0.1, 0.15) * self.Scale, 0.05, 0.35))
	p:SetStartAlpha(255)
	p:SetEndAlpha(0)
	p:SetStartSize(50 * self.Scale)
	p:SetEndSize(50 * self.Scale)
	p:SetColor(255, 220, 200)
	
	--Normal Splash
	for i=1, 5 do
		local p = self.GroundEmitter:Add("sprites/light_ignorez", self.Pos + self.Normal * 2)
		p:SetDieTime(math.Clamp(math.Rand(0.1, 0.15) * self.Scale, 0.05, 0.35))
		p:SetStartSize(50 * self.Scale)
		p:SetEndSize(50 * self.Scale)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetColor(255, 220, 200)
		p:SetAngles(self.Normal:Angle())
	end
	
	self.Emitter:Finish()
	self.GroundEmitter:Finish()
end

function EFFECT:Think( )
  	return false
end

function EFFECT:Render()
	return false
end
