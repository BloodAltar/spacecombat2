--[[*********************************************
	Scalable Ground Breach Effect
	(for when crystals pop out of the ground)
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )
	self.Normal = data:GetNormal()
	self.Pos = data:GetOrigin()
	self.Scale = data:GetScale()
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Rubble
	for i=1, 25 + math.floor(self.Scale * 0.5) do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * math.Rand(75, 350) * (1 + math.log(1 + self.Scale))
		
		local p = self.Emitter:Add("effects/shipsplosion/debris_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(5, 6))
		p:SetVelocity(velocity)
		p:SetCollide(true)
		p:SetBounce(0.25)
		p:SetGravity(Vector(0, 0, -600))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(5 * (math.log(1 + self.Scale)))
		p:SetEndSize(5 * (math.log(1 + self.Scale)))
		p:SetColor(53, 49, 45)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-15, 15))
	end
	
	--Dust
	for i=1, 25 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * (math.Rand(175, 500) * (math.log(1 + self.Scale)))
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(1, 3))
		p:SetAirResistance(150)
		p:SetVelocity(velocity)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(5, 15) * self.Scale)
		p:SetEndSize(math.random(35, 50) * self.Scale)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.5, 0.5))
		p:SetColor(106, 98, 91)
	end
	
  	self.Emitter:Finish()
end

function EFFECT:Think( )
	return false
end

function EFFECT:Render( )
	return false
end
