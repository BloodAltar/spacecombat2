--[[*********************************************
	Scalable, Colorable Repair Beam
	
	Author: Steeveeo & Lt.Brandon
**********************************************]]--

local sparkDist = 2500

local bMats = {}
bMats.Glow23 = Material("sprites/lgtning")
bMats.Glow23:SetMaterialInt("$selfillum", 1)
bMats.Glow23:SetMaterialInt("$illumfactor",1)
bMats.Glow24 = Material("effects/blueflare1")
bMats.Glow24:SetMaterialInt("$selfillum", 10)
bMats.Glow24:SetMaterialInt("$illumfactor",8)
bMats.Glow25 = Material("effects/flares/light-rays_001")
bMats.Glow25:SetMaterialInt("$selfillum", 10)
bMats.Glow25:SetMaterialInt("$illumfactor",8)
--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )
	self.Scale 		= data:GetScale()
	self.ColLerp	= data:GetMagnitude()
	self.Entity		= data:GetEntity()
    self.StartOffset= self.Entity:WorldToLocal(data:GetStart())
	self.LocalAngle	= self.Entity:WorldToLocalAngles(data:GetNormal():Angle())
	self.EndOffset 	= self.Entity:GetRange()
	self.Color 		= Color(Lerp(self.ColLerp, 0, 128), 255, Lerp(self.ColLerp, 0, 128), 255)

	self.CurrentAngle = self.Entity:LocalToWorldAngles(self.LocalAngle):Forward()
	self.StartPos = self.Entity:GetPos() + (self.CurrentAngle * self.StartOffset)
	self.MaxRange = self.EndOffset
	
	self.Emitter = ParticleEmitter(self.StartPos)
	self.NextEmit = CurTime()
	self.IsHitting = false
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	if not IsValid(self.Entity) then return false end
	
	if self.Dying then
		self.Scale = self.Scale - ((self.Scale * 5) * FrameTime())
	end

    self.CurrentAngle = self.Entity:LocalToWorldAngles(self.LocalAngle):Forward()
	self.StartPos = self.Entity:LocalToWorld(self.StartOffset)
	self.EndPos = self.StartPos + (self.CurrentAngle * self.EndOffset)

	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	tracedata.filter = {self.Entity}
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit then
		self.IsHitting = true
		self.EndPos = tr.HitPos
		self.LastHitRange = tr.HitPos:Distance(self.StartPos)
	else
		self.IsHitting = false
		self.LastHitRange = self.Entity:GetRange()
	end
	
	self:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	if not self.Entity:GetShooting() and not self.Dying then
		timer.Simple(1, function() 
			self.Dead = true
		end)
		
		self.Dying = true
	end
	
	if self.Dead then
		self.Emitter:Finish()
		return false
	end
	
	return true
end


--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	local Size = math.random(100, 150) * self.Scale
	local ContactSize = Size
	if self.IsHitting then
		ContactSize = Size * 2
	end
	
	if not self.Dying then
		render.SetMaterial(bMats.Glow24)
	   	render.DrawSprite(self.StartPos, Size, Size, self.Color) 
	   	render.DrawSprite(self.StartPos, Size/2, Size/2, Color(255, 255, 255, 255))
		render.SetMaterial(bMats.Glow25)
	   	render.DrawSprite(self.StartPos, Size, Size, self.Color) 
	   	render.DrawSprite(self.StartPos, Size*2, Size*2, Color(255, 255, 255, 255))
		
		--Emitter Particles
		if self.IsHitting then
			if CurTime() > self.NextEmit and LocalPlayer():GetPos():DistToSqr(self.EndPos) < sparkDist * sparkDist then
				--Sparks Long
				for i=1, 10 do
					local velocity = (-self.CurrentAngle + (VectorRand() * 0.75)):GetNormalized() * (math.Rand(50, 75) * self.Scale)
					
					local p = self.Emitter:Add("effects/shipsplosion/sparks_002", self.EndPos)
					p:SetDieTime(math.Rand(0.15, 0.35))
					p:SetVelocity(velocity)
					p:SetAirResistance(75)
					p:SetStartAlpha(255)
					p:SetEndAlpha(0)
					p:SetStartLength(math.Rand(5, 7.5) * self.Scale)
					p:SetEndLength(p:GetStartLength() + (math.Rand(100, 125) * self.Scale))
					p:SetStartSize(math.Rand(30, 55) * self.Scale)
					p:SetEndSize(p:GetStartSize() + (math.Rand(15, 20) * self.Scale))
					p:SetRoll(math.Rand(0, 360))
					p:SetColor(self.Color.r, self.Color.g, self.Color.b)
				end
				
				self.NextEmit = CurTime() + 0.1
			end
		end
	end

   	local Offset = self.CurrentAngle * self.Scale
	render.SetMaterial(bMats.Glow24)
   	render.DrawSprite(self.EndPos + Offset, ContactSize, ContactSize, self.Color) 
   	render.DrawSprite(self.EndPos + Offset, ContactSize/2, ContactSize/2, Color(255, 255, 255, 255))
	render.SetMaterial(bMats.Glow25)
   	render.DrawSprite(self.EndPos + Offset, ContactSize*2, ContactSize*2, self.Color) 
   	render.DrawSprite(self.EndPos + Offset, ContactSize, ContactSize, Color(255, 255, 255, 255))
	
	render.SetMaterial( bMats.Glow23 )
	local offsetTime = CurTime() * 1.75
   	render.DrawBeam( self.EndPos, self.StartPos, (75 + (15 * math.sin(CurTime() * (25 + math.Rand(15, 25))))) * self.Scale, -0.01 * self.LastHitRange + offsetTime, offsetTime, Color(self.Color.r, self.Color.g, self.Color.b, 32))
   	render.DrawBeam( self.EndPos, self.StartPos, (100 + (15 * math.sin(CurTime() * (25 + math.Rand(3, 5))))) * self.Scale, -0.0025 * self.LastHitRange + offsetTime, offsetTime, Color(self.Color.r, self.Color.g, self.Color.b, 64))
  
	render.SetMaterial( bMats.Glow24 )	
   	render.DrawBeam( self.EndPos, self.StartPos, 40 * self.Scale, 0.5, 0.5, self.Color )
   	render.DrawBeam( self.EndPos, self.StartPos, (20 + (25 * math.sin(CurTime() * 45))) * self.Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
end
