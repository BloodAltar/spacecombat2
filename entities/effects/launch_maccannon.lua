/**********************************************
	Scalable Mac Cannon Fire Effect
	
	Author: Steeveeo
***********************************************/

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.Entity = data:GetEntity()
	self.ElectricDelay = CurTime() + 0.5
	self.DieTime = CurTime() + 4
	self.Emitter = ParticleEmitter(self.Pos)
	
	//Smoke Plume
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.15)):GetNormalized() * (math.Rand(0, 2500) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(1, 3) * self.Scale, 0.5, 8))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(32)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(15, 35) * self.Scale)
		p:SetEndSize(math.random(75, 150) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(math.random(0, 64), math.random(0, 64), math.random(0, 128))
	end
	
	//Shocks
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.15)):GetNormalized() * (math.Rand(0, 2500) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(0.1, 1))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(math.random(35, 75) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(200, 220, 255)
	end
	
	//Sparks
	for i=1, 35 do
		local velocity = (self.Normal + (VectorRand() * 0.35)):GetNormalized() * (math.Rand(0, 1250) * self.Scale)
		
		local p = self.Emitter:Add("effects/flares/light-rays_001", self.Pos)
		p:SetDieTime(math.Rand(0.25, 0.75))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(math.random(35, 50) * self.Scale)
		p:SetEndSize(0)
	end
	
	--Plasma Stream
	for i=1, 15 do
		local velocity = self.Normal * (math.Rand(0, 1250) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(0.1, 0.75))
		p:SetVelocity(velocity)
		p:SetAirResistance(500)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(45, 75) * self.Scale)
		p:SetEndSize(0)
		p:SetStartLength(0)
		p:SetEndLength(math.random(50, 1250) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(144, 144, 255)
	end
	
	//Flare
	for i=1, 2 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001", self.Pos)
		p:SetDieTime(math.Rand(0.25, 0.50))
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(750 * self.Scale)
		p:SetEndSize(0)
		p:SetColor(200, 220, 255)
	end
end

function EFFECT:Think( )
  	if CurTime() > self.DieTime then
		self.Emitter:Finish()
		return false
	else
		if CurTime() > self.ElectricDelay and IsValid(self.Entity) then
			//Shocks
			for i=1, 3 do
				local mins = self.Entity:OBBMins()
				local maxs = self.Entity:OBBMaxs()
				local pos = Vector(math.Rand(mins.x, maxs.x), math.Rand(mins.y, maxs.y), math.Rand(mins.z, maxs.z))
				local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), self.Entity:LocalToWorld(pos))
				p:SetDieTime(math.Rand(0.1, 0.4))
				p:SetGravity(self.Normal * 300 * self.Scale)
				p:SetAirResistance(300)
				p:SetStartAlpha(255)
				p:SetEndAlpha(0)
				p:SetStartSize(0)
				p:SetEndSize(math.random(15, 20) * self.Scale)
				p:SetRoll(math.random(0, 360))
				p:SetRollDelta(math.Rand(-1, 1))
				p:SetColor(255, 255, 200)
			end
		end
	
		return true
	end
end

function EFFECT:Render( )
	return false
end
