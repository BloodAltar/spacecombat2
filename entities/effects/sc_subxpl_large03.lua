--[[*********************************************
	Large Hull Explosion Effect w/ Debris
	
	Author: Steeveeo
**********************************************]]--

local sound_Explosions = {}

sound_Explosions[1] = {}
sound_Explosions[1].Near = Sound("shipsplosion/sc_explode_01_near.mp3")
sound_Explosions[1].Mid = Sound("shipsplosion/sc_explode_01_mid.mp3")
sound_Explosions[1].Far = Sound("shipsplosion/sc_explode_01_far.mp3")

sound_Explosions[2] = {}
sound_Explosions[2].Near = Sound("shipsplosion/sc_explode_02_near.mp3")
sound_Explosions[2].Mid = Sound("shipsplosion/sc_explode_02_mid.mp3")
sound_Explosions[2].Far = Sound("shipsplosion/sc_explode_02_far.mp3")

sound_Explosions[3] = {}
sound_Explosions[3].Near = Sound("shipsplosion/sc_explode_03_near.mp3")
sound_Explosions[3].Mid = Sound("shipsplosion/sc_explode_03_mid.mp3")
sound_Explosions[3].Far = Sound("shipsplosion/sc_explode_03_far.mp3")

sound_Explosions[4] = {}
sound_Explosions[4].Near = Sound("shipsplosion/sc_explode_04_near.mp3")
sound_Explosions[4].Mid = Sound("shipsplosion/sc_explode_04_mid.mp3")
sound_Explosions[4].Far = Sound("shipsplosion/sc_explode_04_far.mp3")

function EFFECT:Init( data )
	self.Normal = data:GetNormal()
	
	self.Pos = data:GetOrigin()
	self.Emitter = ParticleEmitter(self.Pos)
	
	self.Dist = LocalPlayer():GetPos():Distance(self.Pos)
	self.FarDist = GetConVar("sc_shipDeathSoundRadius_Max"):GetFloat()
	self.MidDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_MidFactor"):GetFloat()
	self.NearDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_NearFactor"):GetFloat()
	
	--Debris
	for i=1, 35 do
		local velocity = VectorRand() * math.Rand(300, 1500)
		local size = math.random(20, 50)
		
		local p = self.Emitter:Add("effects/shipsplosion/debris_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(5)
		p:SetVelocity(velocity)
		p:SetAirResistance(50)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(size)
		p:SetEndSize(size)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	--Fire Main
	for i=1, 30 do
		local velocity = VectorRand() * math.Rand(200, 1500)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(4, 6))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(200, 450))
		p:SetEndSize(math.random(500, 800))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end
	
	--Fire Highlights
	for i=1, 20 do
		local velocity = VectorRand() * math.Rand(200, 1500)
		
		local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Pos)
		p:SetDieTime(math.Rand(4, 6))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(200, 450))
		p:SetEndSize(math.random(500, 800))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(96, 96, 255)
	end
	
	--Flare
	for i=1, 2 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(1, 1.5))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(5000)
		p:SetEndSize(1250)
		p:SetColor(255, 220, 200)
	end
	
	--Flare Rays
	for i=1, 3 do
		local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Pos)
		p:SetDieTime(math.Rand(1, 1.5))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(3500)
		p:SetEndSize(3500)
		p:SetColor(0, 0, 255)
	end
	
	--Play Sound
	math.randomseed(CurTime())
	local sound = sound_Explosions[math.random(1, #sound_Explosions)]
	if self.Dist < self.NearDist then
		LocalPlayer():EmitSound(sound.Near, 511, 50, 1)
	elseif self.Dist < self.MidDist then
		LocalPlayer():EmitSound(sound.Mid, 511, 50, 1)
	elseif self.Dist < self.FarDist then
		LocalPlayer():EmitSound(sound.Far, 511, 50, 1)
	end
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		util.ScreenShake(self.Pos, 3, 0.1, 0.5, 4096)
	end
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
	return false
end

function EFFECT:Render( )
	return false
end
