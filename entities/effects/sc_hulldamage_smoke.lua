--[[*********************************************
	Scalable Hull Damage particle (Light Smoke)
	
	Author: Steeveeo
**********************************************]]--

--Pruning thought process for particles so they can go away when the core dies,
--making room for big explosion effects
local function particleThink(p)
	if p.HadCore and not IsValid(p.Core) then
		p:SetLifeTime(1e30)
	else
		p:SetNextThink(CurTime())
	end
end

function EFFECT:Init( data )
	self.Core = data:GetEntity()
	self.HadCore = IsValid(self.Core)
	self.Angle = self.Core:WorldToLocalAngles(data:GetNormal():Angle())
	self.Pos = self.Core:WorldToLocal(data:GetOrigin())
	self.Scale = data:GetScale()
	self.CutPercent = data:GetMagnitude() --Cut out if core is above this percent health
	self.LifeTime = 20
	self.Life = 0
	self.Emitter = ParticleEmitter(self.Core:LocalToWorld(self.Pos))
	
	self.NextEmit = CurTime()
end

function EFFECT:Think( )
	if self.Emitter == nil then return false end --Garry why...

	if not IsValid(self.Core) then
		self.Emitter:Finish()
		return false
	end
	
	self.Life = self.Life + FrameTime()
	
	--Remove after LifeTime seconds if we're not tracking a core
	if self.Core:GetClass() ~= "ship_core" then
		self.LifeTime = self.LifeTime - FrameTime()
		if self.LifeTime <= 0 then
			self.Emitter:Finish()
			return false
		end
	--Remove only if Armor and Hull are above CutPercent percent (after living for at least 3 seconds)
	else
		if self.Life > 3 then
			local maxHP = self.Core:GetArmorMax() + self.Core:GetHullMax() --Using this method instead of Percent to deal with Hull Tanks
			local curHP = self.Core:GetArmorAmount() + self.Core:GetHullAmount()
			local percent = (curHP / maxHP) * 100
			
			if percent >= self.CutPercent then
				self.Emitter:Finish()
				return false
			end
		end
	end
	
	if CurTime() > self.NextEmit then
		local velocity = (self.Core:LocalToWorldAngles(self.Angle):Forward() + (VectorRand() * 0.25)):GetNormalized() * (math.Rand(40, 50) * (1 + math.log(1 + self.Scale)))
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_001", self.Core:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(1.5, 2) * (1 + math.log(1 + self.Scale)))
		p:SetVelocity(velocity)
		p:SetGravity(Vector(10, 10, 0) * (1 + math.log(1 + self.Scale / 10)))
		p:SetStartAlpha(64)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(5, 10) * self.Scale)
		p:SetEndSize(math.random(45, 65) * self.Scale)
		p:SetColor(64, 64, 64)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetThinkFunction(particleThink)
		p:SetNextThink(CurTime())
		p.Core = self.Core
		p.HadCore = self.HadCore
		
		self.NextEmit = CurTime() + 0.1
	end
	
	return true
end


function EFFECT:Render( )
	
	
	return false
					 
end
